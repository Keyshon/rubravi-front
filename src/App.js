import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import ScrollToTop from 'ScrollToTop';
import 'styles/App.scss';

import Home from 'pages/home';
import Products from 'pages/products';
import ProductsOrder from 'pages/products-order';
import ContactUs from 'pages/contact-us';
import AboutUs from 'pages/about-us';
import Partners from 'pages/partners';
import PrivacyPolicy from 'pages/privacy-policy';
import TermsOfUse from 'pages/terms-of-use';
import Disclaimer from 'pages/disclaimer';
import Page404 from 'pages/404';

class App extends React.Component {
  render() {
    return (
      <Router onUpdate={() => window.scrollTo(0, 0)}>
        <ScrollToTop>
          <div className='App'>
            <Switch>
              <Route path='/' exact component={Home} />
              <Route path='/home' exact component={Home} />
              <Route path='/products' exact component={Products} />
              <Route path='/products/order' exact component={ProductsOrder} />
              <Route path='/contact' exact component={ContactUs} />
              <Route path='/about' exact component={AboutUs} />
              <Route path='/partners' exact component={Partners} />
              <Route path='/terms-of-use' exact component={TermsOfUse} />
              <Route path='/privacy-policy' exact component={PrivacyPolicy} />
              <Route path='/disclaimer' exact component={Disclaimer} />
              <Route path='*' exact component={Page404} />
            </Switch>
          </div>
        </ScrollToTop>
      </Router>
    );
  }
}

export default App;
