import React from 'react';

import './ModalJoin.scss';
import CloseSVG from 'resources/svg/close-btn.svg';

import ModalJoinForm from './ModalJoinForm';

class ModalJoin extends React.Component {
  constructor() {
    super();
    this.ieStyle = this.ieStyle.bind(this);
  }

  ieStyle() {
    if (
      window.navigator.userAgent.indexOf('MSIE') !== -1 ||
      window.navigator.userAgent.indexOf('Trident/') !== -1
    )
      return { backgroundColor: 'transparent' };
    else return {};
  }

  render() {
    return (
      <div
        className='modal-wrapper'
        style={this.props.isOpen ? {} : { display: 'none' }}
        onClick={this.props.switchHandler}
      >
        <div className='modal-content-wrapper'>
          <div className='modal-content-bg'>
            <div className='modal-content'>
              <div className='modal-heading'>
                <h2>Make an enquiry</h2>
                <div
                  className='modal-heading__close-button'
                  style={this.ieStyle()}
                >
                  {(() => {
                    if (
                      window.navigator.userAgent.indexOf('MSIE') !== -1 ||
                      window.navigator.userAgent.indexOf('Trident/') !== -1
                    ) {
                      return (
                        <image
                          src={CloseSVG}
                          className='modal-heading__close-button_image'
                        />
                      );
                    }
                  })()}
                </div>
              </div>
              <div className='modal-form'>
                <ModalJoinForm />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ModalJoin;
