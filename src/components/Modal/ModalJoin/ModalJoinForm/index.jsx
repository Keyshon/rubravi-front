import React from 'react';

import './ModalJoinForm.scss';
import Treat from 'resources/images/banner/scalable_apps.png';
import Form from 'components/Form';

class ModalJoinForm extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div className='modal-form'>
        <div className='modal-form__content'>
          <Form />
        </div>
        <div className='modal-form__image-block'>
          <div className='modal-form__image'>
            <img src={Treat} alt='' />
          </div>
        </div>
      </div>
    );
  }
}

export default ModalJoinForm;
