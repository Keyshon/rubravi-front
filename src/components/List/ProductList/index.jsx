import React from 'react';
import ReactDOM from 'react-dom';

import 'styles/App.scss';
import './ProductList.scss';
import colors from 'styles/colors.scss';

import Button from 'components/Button';

class ProductList extends React.Component {
  constructor() {
    super();
    this.state = {
      smoothOpacity: 0
    };
    this.darken = this.darken.bind(this);
    this.calculateOpacity = this.calculateOpacity.bind(this);
  }

  darken(amount) {
    const alpha = amount < 0 || amount > 100 ? 0 : amount;
    if (alpha > 0) {
      return {
        backgroundColor: `rgba(0, 0, 0, ${alpha / 100})`
      };
    } else return {};
  }

  calculateOpacity() {
    const rect = ReactDOM.findDOMNode(this).getBoundingClientRect();
    let opacity = 0;
    let newPos;
    const newHeight = Math.min(
      rect.height,
      Math.abs(window.innerHeight - rect.height)
    );
    if (rect.height > window.innerHeight) newPos = -rect.y + newHeight;
    else newPos = -rect.y + 2 * newHeight;
    if (newPos > newHeight || navigator.userAgent.indexOf('Edge') !== -1)
      opacity = 1;
    else if (newPos < 0) opacity = 0;
    else {
      opacity = newPos / newHeight;
    }
    if (isNaN(opacity)) opacity = 1;
    this.setState({
      smoothOpacity: opacity
    });
  }

  componentDidMount() {
    this.calculateOpacity();
    window.addEventListener('scroll', this.calculateOpacity);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.calculateOpacity);
  }

  render() {
    return (
      <div className='content-container-bg alter-bg'>
        <div style={this.darken(this.props.darken)}>
          <div
            className='content-container-wrapper'
            style={this.props.smooth ? { paddingTop: 0 } : {}}
          >
            <div
              className='content-container'
              style={
                this.props.smooth ? { opacity: this.state.smoothOpacity } : {}
              }
            >
              <h2
                style={{ color: colors.colorWhite }}
                className='content-container__title'
              >
                {this.props.title}
              </h2>
              <div className='product-list'>
                {this.props.items.map(item => {
                  return (
                    <div className='product-list__item' key={item.key}>
                      <a href={item.link}>
                        <div
                          className='product-list-item-logo'
                          style={
                            item.color ? { backgroundColor: item.color } : {}
                          }
                        >
                          <img src={item.image} alt='' />
                        </div>
                      </a>
                      <div className='product-list-item-content-block'>
                        <a href={item.link}>
                          <h3 className='product-list-item-name'>
                            {item.name}
                          </h3>
                        </a>
                        <div
                          className='product-list-item-text'
                          style={!item.buttonText ? { paddingBottom: 0 } : {}}
                        >
                          {item.text}
                        </div>
                        <div
                          className='product-list-item-button'
                          style={!item.buttonText ? { display: 'none' } : {}}
                        >
                          <Button
                            text={item.buttonText}
                            color={item.buttonColor}
                            link={item.buttonLink}
                            wide
                          />
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductList;
