import React from 'react';
import ReactDOM from 'react-dom';

import 'styles/App.scss';
import './AdvantageList.scss';
import colors from 'styles/colors.scss';
import media from 'styles/media.scss';

import Button from 'components/Button';

class AdvantageList extends React.Component {
  constructor() {
    super();
    this.state = {
      smoothOpacity: 0,
      textSmoothOpacity: 0,
      mobile: false
    };
    this.iconStyles = this.iconStyles.bind(this);
    this.darken = this.darken.bind(this);
    this.background = this.background.bind(this);
    this.whitenText = this.whitenText.bind(this);
    this.titleStyles = this.titleStyles.bind(this);
    this.finalStyle = this.finalStyle.bind(this);
    this.calculateOpacity = this.calculateOpacity.bind(this);
    this.resizeComponent = this.resizeComponent.bind(this);
    this.ieStyle = this.ieStyle.bind(this);
  }

  ieStyle() {
    if (
      window.navigator.userAgent.indexOf('MSIE') !== -1 ||
      window.navigator.userAgent.indexOf('Trident/') !== -1
    )
      return { backgroundColor: 'transparent' };
    else return {};
  }

  iconStyles(iconPath, bgColor) {
    if (!bgColor) bgColor = colors.colorNeonBright;
    if (this.props.darken) bgColor = colors.colorWhite;
    if (this.props.advantage) {
      if (iconPath) {
        return {
          WebkitMask: `url(${iconPath}) no-repeat center`,
          mask: `url(${iconPath}) no-repeat center`,
          WebkitMaskSize: 'cover',
          backgroundColor: bgColor
        };
      } else return {};
    } else {
      return {
        color: bgColor,
        fontSize: !this.state.mobile ? 84 : 72,
        fontWeight: 1000,
        display: 'block',
        opacity: this.props.darken ? this.props.darken / 100 : 1
      };
    }
  }

  darken(amount) {
    const alpha = amount < 0 || amount > 100 ? 0 : amount;
    if (alpha > 0) {
      return {
        backgroundColor: `rgba(0, 0, 0, ${alpha / 100})`
      };
    } else return {};
  }

  background(imagePath) {
    if (imagePath) {
      if (this.props.parallax) {
        return {
          backgroundImage: `url(${imagePath})`,
          backgroundAttachment: 'fixed',
          backgroundPosition: 'center',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover'
        };
      }
      return {
        backgroundImage: `url(${imagePath})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover'
      };
    } else return {};
  }

  whitenText() {
    return this.props.darken ? { color: colors.colorWhite } : {};
  }

  titleStyles() {
    return this.props.advantage
      ? {
          transform: 'translateY(-20%)',
          WebkitTransform: 'translateY(-20%)',
          MozTransform: 'translateY(-20%)',
          OTransform: 'translateY(-20%)',
          MsTransform: 'translateY(-20%)'
        }
      : { marginLeft: 70 };
  }

  marginStyle(index) {
    if (!this.state.mobile) {
      // Desktop
      const even = this.props.items.length % 2 === 0;
      const indexStatus = index % (even ? 2 : 3);
      const hIndentStyle = indexStatus === 0 ? { marginRight: 0 } : {};
      const widthStyle = even ? { minWidth: '45%' } : { minWidth: '28%' };
      const applyVIndentStyle = even
        ? this.props.items.length - index >= 2
          ? true
          : false
        : this.props.items.length - index >= 3
        ? true
        : false;
      const vIndentStyle = applyVIndentStyle ? { marginBottom: 30 } : {};

      return { ...hIndentStyle, ...vIndentStyle, ...widthStyle };
    } else {
      // Mobile
      const hIndentStyle = { marginRight: 0 };
      const widthStyle = { minWidth: '100%' };
      const vIndentStyle = { marginBottom: 30 };

      return { ...hIndentStyle, ...vIndentStyle, ...widthStyle };
    }
  }

  smoothText() {
    return this.props.smooth ? { opacity: this.state.textSmoothOpacity } : {};
  }

  finalStyle() {
    const alpha = this.state.smoothOpacity;
    const bigAlpha = this.state.smoothOpacity * 3;
    let gradientStyle = {
      WebkitMaskImage: `linear-gradient(rgba(0,0,0,0), rgba(0,0,0,${alpha}), rgba(0,0,0,${bigAlpha}))`,
      MaskImage: `linear-gradient(rgba(0,0,0,0), rgba(0,0,0,${alpha}), rgba(0,0,0,${bigAlpha}))`
    };
    if (
      window.navigator.userAgent.indexOf('MSIE') !== -1 ||
      window.navigator.userAgent.indexOf('Trident/') !== -1
    )
      gradientStyle = { opacity: 0 };
    return { ...gradientStyle };
  }

  calculateOpacity() {
    const rect = ReactDOM.findDOMNode(this).getBoundingClientRect();
    let opacity = 0;
    const newHeight =
      rect.height - window.innerHeight > 0
        ? rect.height - window.innerHeight
        : 0;
    if (rect.y > -newHeight) opacity = 1;
    else if (rect.y < -rect.height) opacity = 0;
    else
      opacity =
        (rect.y + rect.height) / Math.min(rect.height, window.innerHeight);
    if (isNaN(opacity)) opacity = 1;
    this.setState({
      smoothOpacity: 1 - opacity,
      textSmoothOpacity: opacity
    });
  }

  resizeComponent() {
    if (window.innerWidth >= parseInt(media.tablet)) {
      this.setState({
        mobile: false
      });
    } else {
      this.setState({
        mobile: true
      });
    }
  }

  componentDidMount() {
    this.resizeComponent();
    window.addEventListener('resize', this.resizeComponent);
    if (this.props.smooth) {
      this.calculateOpacity();
      window.addEventListener('scroll', this.calculateOpacity);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeComponent);
    window.removeEventListener('scroll', this.calculateOpacity);
  }

  render() {
    return (
      <div
        className='content-container-bg'
        style={this.background(this.props.background)}
      >
        <div style={this.darken(this.props.darken)}>
          <div className='content-container-bg-image'>
            <div
              className='content-container-wrapper'
              style={{ ...this.whitenText(), ...this.smoothText() }}
            >
              <div className='content-container'>
                <h2
                  className='content-container__title'
                  style={this.whitenText()}
                >
                  {this.props.title}
                </h2>
                <div className='advantage-list'>
                  {this.props.items.map((item, index) => {
                    return (
                      <div
                        key={item.key}
                        className='advantage-list__item'
                        style={this.marginStyle(index + 1)}
                      >
                        <div className='item-title-container'>
                          <div
                            className='item-logo-container'
                            style={{
                              ...this.iconStyles(item.logo, item.logoColor),
                              ...this.ieStyle()
                            }}
                          >
                            {(() => {
                              if (
                                (window.navigator.userAgent.indexOf('MSIE') !==
                                  -1 ||
                                  window.navigator.userAgent.indexOf(
                                    'Trident/'
                                  ) !== -1) &&
                                this.props.advantage
                              ) {
                                return (
                                  <img
                                    src={item.logo}
                                    alt=''
                                    className='item-logo-image'
                                  />
                                );
                              }
                            })()}
                            {this.props.advantage ? '' : index + 1}
                          </div>
                          <div className='item-title-text'>
                            <h3
                              style={{
                                ...this.whitenText(),
                                ...this.titleStyles()
                              }}
                            >
                              {item.title}
                            </h3>
                          </div>
                        </div>
                        {item.text}
                      </div>
                    );
                  })}
                </div>
                <div
                  className='advantage-list-button-block'
                  style={this.props.buttonText ? {} : { height: '0px' }}
                >
                  <div className='advantage-list-button'>
                    <Button
                      text={this.props.buttonText}
                      color={
                        this.props.buttonColor ? this.props.buttonColor : 'neon'
                      }
                      link={this.props.buttonLink}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            className='advantage-list-smooth-section advantage-list-gradient'
            style={this.props.smooth ? {} : { display: 'none' }}
          />
        </div>
        <div
          className='advantage-list-smooth-transition advantage-list-bg'
          style={this.props.smooth ? this.finalStyle() : { display: 'none' }}
        />
      </div>
    );
  }
}

export default AdvantageList;
