import React from 'react';

import './Hint.scss';
import colors from 'styles/colors.scss';

class Hint extends React.Component {
  constructor() {
    super();
    this.state = {
      isOpen: false
    };
    this.showModal = this.showModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.activeStyles = this.activeStyles.bind(this);
  }

  closeModal(e) {
    if (e.target.className.indexOf('modal-item') !== -1) return;
    this.setState({
      isOpen: false
    });
    window.removeEventListener('click', this.closeModal);
  }

  showModal(e) {
    if (e.target.className.indexOf('hint-wrapper') == -1) return;
    const newIsOpen = !this.state.isOpen;
    this.setState({
      isOpen: newIsOpen
    });
    if (newIsOpen)
      setTimeout(() => window.addEventListener('click', this.closeModal), 300);
    else window.removeEventListener('click', this.closeModal);
  }

  activeStyles() {
    if (!this.props.white) {
      if (this.state.isOpen) {
        return {
          color: colors.colorBlueHover,
          borderBottom: `2px dashed ${colors.colorBlueHover}`
        };
      } else {
        return {
          color: colors.colorBlack,
          borderBottom: `2px dashed ${colors.colorBlack}`
        };
      }
    } else {
      if (this.state.isOpen) {
        return {
          color: colors.colorGrayBrightest,
          borderBottom: `2px dashed ${colors.colorGrayBrightest}`
        };
      } else {
        return {
          color: colors.colorWhite,
          borderBottom: `2px dashed ${colors.colorWhite}`
        };
      }
    }
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.closeModal);
  }

  render() {
    return (
      <span className='hint' onClick={this.showModal}>
        <span
          className='modal-item'
          style={this.state.isOpen ? { opacity: 1 } : { opacity: 0 }}
        >
          {this.props.hintText}
        </span>
        <span className='hint-wrapper' style={this.activeStyles()}>
          {this.props.children}
        </span>
      </span>
    );
  }
}

export default Hint;
