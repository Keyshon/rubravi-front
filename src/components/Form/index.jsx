import React from 'react';

import './Form.scss';
import media from 'styles/media.scss';

import PhoneField from 'components/Field/PhoneField';
import SliderField from 'components/Field/SliderField';
import TextField from 'components/Field/TextField';
import SelectField from 'components/Field/SelectField';
import Button from 'components/Button';

import { connect } from 'react-redux';

import postOrder from 'api/ProductOrders';
import getSubscription from 'api/SubscriptionTypes';
import { getProducts } from 'api/ProductTypes';

require('formdata-polyfill');

class Form extends React.Component {
  constructor() {
    super();
    this.state = {
      showForm: true,
      serverError: '',
      subscriptionIds: [],
      subscriptionNames: [],
      productsIds: [],
      productsNames: [],
      responseiveSectionStyles: {},
      responseiveElementStyles: {}
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.showMessage = this.showMessage.bind(this);
    this.resizeComponent = this.resizeComponent.bind(this);

    this.fields = [React.createRef(), React.createRef(), React.createRef()];
  }

  componentDidMount() {
    // Get subscriptions
    const subIds = [];
    const subNames = [];
    getSubscription()
      .then(response => {
        return response.json();
      })
      .then(data => {
        for (let i = 0; i < data.length; i++) {
          // The last type is custom. Remove it.
          subIds.push(data[i]._id);
          subNames.push(data[i].term);
        }

        this.setState({
          subscriptionIds: subIds,
          subscriptionNames: subNames
        });
      });

    // Get products
    const prodIds = [];
    const prodNames = [];
    getProducts()
      .then(response => {
        return response.json();
      })
      .then(data => {
        for (let i = 0; i < data.length; i++) {
          prodIds.push(data[i]._id);
          prodNames.push(data[i].name);
        }

        this.setState({
          productsIds: prodIds,
          productsNames: prodNames
        });
      });
    window.addEventListener('resize', this.resizeComponent);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeComponent);
  }

  resizeComponent() {
    if (this.props.short) {
      if (window.innerWidth >= parseInt(media.tablet))
        this.setState({
          responseiveSectionStyles: { display: 'flex' },
          responseiveElementStyles: { marginRight: '30px' }
        });
      else {
        this.setState({
          responseiveSectionStyles: { display: 'block' },
          responseiveElementStyles: { marginRight: '0' }
        });
      }
    }
  }

  showMessage(response, message) {
    switch (response.status) {
      case 201:
        this.setState({ showForm: false });
        break;
      case 400:
        this.setState({
          serverError: `Validation is not successful: ${message.status}`
        });
        break;
      case 500:
        this.setState({
          serverError: 'Server is temporary unavailable. Please, try later.'
        });
        break;
      default:
        this.setState({
          serverError: 'Server is temporary unavailable. Please, try later.'
        });
        break;
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    // Validate all fields
    let executeSubmit = true;
    for (let i = 0; i < this.fields.length; i++) {
      let c = this.fields[i].current.validate();
      if (c !== '') executeSubmit = false;
    }
    if (!executeSubmit) return null;

    // Send request
    let res;
    const data = new FormData(event.target);
    postOrder(
      this.props.product,
      data.get('userName'),
      data.get('userPhone'),
      data.get('userEmail'),
      this.props.subscription,
      this.props.term
    )
      .then(response => {
        res = response;
        return response.json();
      })
      .then(data => {
        this.showMessage(res, data);
      });
  }

  render() {
    if (this.state.showForm) {
      return (
        <>
          <div
            className='form__server-message'
            style={
              this.state.serverError !== ''
                ? { display: 'block' }
                : { display: 'none' }
            }
          >
            {this.state.serverError !== '' && this.state.serverError}
          </div>
          <form onSubmit={this.handleSubmit}>
            <div
              className='form-field-set'
              style={this.state.responseiveSectionStyles}
            >
              <div
                className='form-field-set__element'
                style={this.props.short ? { display: 'none' } : {}}
              >
                <SelectField
                  name='productType'
                  type='text'
                  placeholder='Product'
                  ids={this.state.productsIds}
                  names={this.state.productsNames}
                  state={this.props.product}
                  dispatch={this.props.setProduct}
                />
              </div>
              <div
                className='form-field-set__element'
                style={this.state.responseiveElementStyles}
              >
                <TextField
                  name='userName'
                  type='text'
                  placeholder='Full name'
                  checkEmpty
                  minLength={5}
                  maxLength={40}
                  ref={this.fields[0]}
                />
              </div>
              <div
                className='form-field-set__element'
                style={this.state.responseiveElementStyles}
              >
                <PhoneField
                  name='userPhone'
                  type='text'
                  placeholder='Phone number'
                  checkEmpty
                  minLength={8}
                  maxLength={20}
                  ref={this.fields[1]}
                />
              </div>
              <div className='form-field-set__element'>
                <TextField
                  name='userEmail'
                  type='text'
                  placeholder='Email'
                  checkEmpty
                  minLength={8}
                  maxLength={40}
                  ref={this.fields[2]}
                />
              </div>
              <div
                className='form-field-set__element'
                style={this.props.short ? { display: 'none' } : {}}
              >
                <SliderField
                  name='subscriptionTerm'
                  aliasName='subscriptionType'
                  type='text'
                  placeholder='Subscription Term'
                  unit='month'
                  ids={this.state.subscriptionIds}
                  names={this.state.subscriptionNames}
                  availableValues={this.state.subscriptions}
                  state={this.props.subscription}
                  dispatch={this.props.setSubscription}
                  stateTerm={this.props.term}
                  dispatchTerm={this.props.setTerm}
                />
              </div>
            </div>
            <div className='form-field-button'>
              <Button text='Send' color='blue' />
            </div>
          </form>
        </>
      );
    } else {
      return (
        <div className='form__success-message'>Thank you for your request</div>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    product: state.product,
    subscription: state.subscription,
    term: state.term
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setProduct: value => {
      dispatch({
        type: 'SET_PRODUCT',
        payload: value
      });
    },
    setTerm: value => {
      dispatch({
        type: 'SET_TERM',
        payload: value
      });
    },
    setSubscription: value => {
      dispatch({
        type: 'SET_SUBSCRIPTION',
        payload: value
      });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form);
