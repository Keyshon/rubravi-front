import React from 'react';

import 'styles/App.scss';
import './Container.scss';

class Container extends React.Component {
  render() {
    return (
      <div className='content-container-bg'>
        <div
          className='content-container-wrapper container-text'
          style={this.props.short ? { paddingBottom: 0 } : {}}
        >
          <div className='content-container'>{this.props.children}</div>
        </div>
      </div>
    );
  }
}

export default Container;
