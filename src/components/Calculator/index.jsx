import React from 'react';

import './Calculator.scss';

import { connect } from 'react-redux';

import Form from 'components/Form';
import SelectField from 'components/Field/SelectField';
import SliderField from 'components/Field/SliderField';

import getSubscription from 'api/SubscriptionTypes';
import { getProducts, getProduct } from 'api/ProductTypes';

class Calculator extends React.Component {
  constructor() {
    super();
    this.state = {
      subscriptionIds: [],
      subscriptionNames: [],
      productsIds: [],
      productsNames: [],
      monthCost: 0,
      totalCost: 0,
      availableCosts: {}
    };
    this.fields = [React.createRef(), React.createRef()];
  }

  componentDidMount() {
    // Get subscriptions
    const subIds = [];
    const subNames = [];
    getSubscription()
      .then(response => {
        return response.json();
      })
      .then(data => {
        for (let i = 0; i < data.length; i++) {
          // The last type is custom. Remove it.
          subIds.push(data[i]._id);
          subNames.push(data[i].term);
        }

        this.setState({
          subscriptionIds: subIds,
          subscriptionNames: subNames
        });
      });

    // Get products
    const prodIds = [];
    const prodNames = [];
    let costs = {};
    getProducts()
      .then(response => {
        return response.json();
      })
      .then(data => {
        for (let i = 0; i < data.length; i++) {
          prodIds.push(data[i]._id);
          prodNames.push(data[i].name);
          costs[data[i]._id] = data[i].costMonth;
        }

        this.setState({
          productsIds: prodIds,
          productsNames: prodNames,
          availableCosts: costs
        });
      });
  }

  calculateMonthPayment() {
    return this.state.availableCosts[this.props.product]
      ? this.state.availableCosts[this.props.product]
      : 0;
  }

  calculateTotalPayment() {
    return this.state.availableCosts[this.props.product]
      ? this.props.term * this.state.availableCosts[this.props.product]
      : 0;
  }

  render() {
    return (
      <>
        <h2>Choose your perfect plan</h2>
        <div className='calculator-wrapper'>
          <div className='calculator-fields-section'>
            <SelectField
              name='productType'
              type='text'
              placeholder='Product'
              ids={this.state.productsIds}
              names={this.state.productsNames}
              state={this.props.product}
              dispatch={this.props.setProduct}
            />
            <SliderField
              name='subscriptionTerm'
              aliasName='subscriptionType'
              type='text'
              placeholder='Subscription Term'
              unit='month'
              ids={this.state.subscriptionIds}
              names={this.state.subscriptionNames}
              availableValues={this.state.subscriptions}
              state={this.props.subscription}
              dispatch={this.props.setSubscription}
              stateTerm={this.props.term}
              dispatchTerm={this.props.setTerm}
            />
          </div>
          <div className='calculator-result-section'>
            <div className='calculator-result calculator-result-month'>
              <div className='calculator-result-description'>Month Payment</div>
              {this.calculateMonthPayment()}$
            </div>
            <div className='calculator-result calculator-result-total'>
              <div className='calculator-result-description'>Total Payment</div>
              {this.calculateTotalPayment()}$
            </div>
          </div>
        </div>
        <Form short />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    product: state.product,
    subscription: state.subscription,
    term: state.term
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setProduct: value => {
      dispatch({
        type: 'SET_PRODUCT',
        payload: value
      });
    },
    setTerm: value => {
      dispatch({
        type: 'SET_TERM',
        payload: value
      });
    },
    setSubscription: value => {
      dispatch({
        type: 'SET_SUBSCRIPTION',
        payload: value
      });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Calculator);
