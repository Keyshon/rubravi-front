import React from 'react';
import { Link } from 'react-router-dom';
import FBLogo from 'resources/svg/media/facebook.svg';
import InstagramLogo from 'resources/svg/media/instagram.svg';
import TwitterLogo from 'resources/svg/media/twitter.svg';

import './FooterBottomSection.scss';
import Logo from 'resources/logo/logo_small.png';

class FooterBottomSection extends React.Component {
  constructor() {
    super();
    this.currentYear = this.currentYear.bind(this);
    this.ieStyle = this.ieStyle.bind(this);
  }

  ieStyle() {
    if (
      window.navigator.userAgent.indexOf('MSIE') !== -1 ||
      window.navigator.userAgent.indexOf('Trident/') !== -1
    )
      return { backgroundColor: 'transparent' };
    else return {};
  }

  currentYear() {
    let d = new Date();
    return d.getFullYear();
  }

  render() {
    return (
      <div className='general-info-wrapper'>
        <div className='general-info-content footer-content'>
          <div className='footer-indent-block'>
            <div className='footer-logo'>
              <div className='footer-logo__image'>
                <Link to='/'>
                  <img src={Logo} alt='' />
                </Link>
              </div>
              <div className='footer-logo__text'>
                <span>© {this.currentYear()} Rubravi</span>
                <span className='footer-company-info'>
                  <span className='footer-company-info__separator'>|</span>
                  <Link to='/terms-of-use'>Tems of Use</Link>
                  <Link to='/privacy-policy'>Privacy Policy</Link>
                  <Link to='/disclaimer'>Disclaimer</Link>
                </span>
              </div>
            </div>
            <div className='footer-general-info'>
              <div className='footer-general-info__media'>
                <a
                  href='https://facebook.com'
                  target='_blank'
                  rel='noopener noreferrer'
                >
                  <div
                    className='footer-media footer-media_facebook'
                    style={this.ieStyle()}
                  >
                    {(() => {
                      if (
                        window.navigator.userAgent.indexOf('MSIE') !== -1 ||
                        window.navigator.userAgent.indexOf('Trident/') !== -1
                      ) {
                        return <image src={FBLogo} />;
                      }
                    })()}
                  </div>
                </a>
                <a
                  href='https://instagram.com'
                  target='_blank'
                  rel='noopener noreferrer'
                >
                  <div
                    className='footer-media footer-media_instagram'
                    style={this.ieStyle()}
                  >
                    {(() => {
                      if (
                        window.navigator.userAgent.indexOf('MSIE') !== -1 ||
                        window.navigator.userAgent.indexOf('Trident/') !== -1
                      ) {
                        return <image src={InstagramLogo} />;
                      }
                    })()}
                  </div>
                </a>
                <a
                  href='https://twitter.com'
                  target='_blank'
                  rel='noopener noreferrer'
                >
                  <div
                    className='footer-media footer-media_twitter'
                    style={this.ieStyle()}
                  >
                    {(() => {
                      if (
                        window.navigator.userAgent.indexOf('MSIE') !== -1 ||
                        window.navigator.userAgent.indexOf('Trident/') !== -1
                      ) {
                        return <image src={TwitterLogo} />;
                      }
                    })()}
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FooterBottomSection;
