import React from 'react';
import { Link } from 'react-router-dom';

import './FooterColumn.scss';

class FooterColumn extends React.Component {
  render() {
    return (
      <div>
        <div className='footer-column__header'>
          <h4>{this.props.title}</h4>
        </div>
        <div className='footer-column__content'>
          <ul>
            {this.props.elements.map(item => {
              return (
                <li key={item.key}>
                  <Link to={item.link}>{item.name}</Link>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
}

export default FooterColumn;
