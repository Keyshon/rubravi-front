import React from 'react';

import './FooterTopSection.scss';
import FooterColumn from './FooterColumn';

class FooterTopSection extends React.Component {
  constructor() {
    super();
    this.state = {
      columnData: [
        {
          key: 0,
          title: 'Security',
          columns: [
            {
              key: 0,
              name: 'VPS Standard',
              link: '/products'
            },
            {
              key: 1,
              name: 'VPS Pro',
              link: '/products'
            },
            {
              key: 2,
              name: 'Building security together',
              link: '/partners'
            },
            {
              key: 3,
              name: 'Private clouds for Business',
              link: '/products'
            }
          ]
        },
        {
          key: 1,
          title: 'Network',
          columns: [
            {
              key: 0,
              name: 'VPN Server',
              link: '/products'
            },
            {
              key: 1,
              name: 'Fast Proxy Connection',
              link: '/products'
            },
            {
              key: 2,
              name: 'Speed and stability',
              link: '/products'
            }
          ]
        },
        {
          key: 2,
          title: 'Cloud Computing',
          columns: [
            {
              key: 0,
              name: 'Big Data',
              link: '/products'
            },
            {
              key: 1,
              name: 'Rendering and Raytracing',
              link: '/products'
            },
            {
              key: 2,
              name: 'Gaming',
              link: '/products'
            },
            {
              key: 3,
              name: 'Virtual machines',
              link: '/products'
            },
            {
              key: 4,
              name: 'Custom needs',
              link: '/products'
            }
          ]
        }
      ]
    };
  }

  render() {
    return (
      <div className='sitemap-wrapper'>
        <div className='sitemap footer-content'>
          <div className='footer-indent-block'>
            {this.state.columnData.map(item => {
              return (
                <div className='sitemap-column' key={item.key}>
                  <FooterColumn title={item.title} elements={item.columns} />
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default FooterTopSection;
