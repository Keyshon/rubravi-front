import React from 'react';

import './Footer.scss';

import FooterBottomSection from './FooterBottomSection';
import FooterTopSection from './FooterTopSection';

class Footer extends React.Component {
  constructor() {
    super();
    this.state = {
      fixed: false
    };
  }

  componentDidMount() {
    const height = document.getElementsByClassName('content-wrapper')[0]
      .scrollHeight;
    const headerHeight = document.getElementsByClassName('header-wrapper')[0]
      .scrollHeight;
    this.setState({ fixed: height + headerHeight < window.outerHeight });
  }

  render() {
    return (
      <div
        className='footer'
        style={
          this.props.isFixed !== false && this.state.fixed
            ? { position: 'fixed', bottom: 0 }
            : {}
        }
      >
        <FooterTopSection />
        <FooterBottomSection />
      </div>
    );
  }
}

export default Footer;
