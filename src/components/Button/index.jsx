import React from 'react';
import { Link } from 'react-router-dom';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import './Button.scss';
import colors from 'styles/colors.scss';

class Button extends React.Component {
  constructor() {
    super();
    this.availableColor = this.availableColor.bind(this);
    this.linkedButton = this.linkedButton.bind(this);
  }

  availableColor() {
    if (this.props.color === 'blue') {
      return {
        backgroundColor: colors.colorBlue,
        border: '1px solid ' + colors.colorBlueBorder
      };
    } else if (this.props.color === 'lightBlue') {
      return {
        backgroundColor: colors.colorBlueBright,
        border: '1px solid ' + colors.colorBlueBorder
      };
    } else {
      return {
        backgroundColor: colors.colorNeonBright,
        border: '1px solid ' + colors.colorNeonAccent
      };
    }
  }

  buttonWidth() {
    let w = {};
    if (this.props.wide) w = { width: '100%', display: 'block' };
    else if (this.props.double) w = { width: '200%', display: 'block' };
    return w;
  }

  buttonStyle() {
    return { ...this.availableColor(), ...this.buttonWidth() };
  }

  linkedButton() {
    if (this.props.link) {
      if (this.props.link.indexOf('#') === 0) {
        return (
          <AnchorLink href={this.props.link}>
            <button type='button' style={this.buttonStyle()}>
              {this.props.text}
            </button>
          </AnchorLink>
        );
      }
      return (
        <Link to={this.props.link}>
          <button type='button' style={this.buttonStyle()}>
            {this.props.text}
          </button>
        </Link>
      );
    } else {
      return (
        <button type='submit' value='submit' style={this.buttonStyle()}>
          {this.props.text}
        </button>
      );
    }
  }

  render() {
    if (this.props.text) {
      return this.linkedButton();
    } else {
      return null;
    }
  }
}

export default Button;
