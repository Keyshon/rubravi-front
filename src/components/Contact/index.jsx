import React from 'react';

import './Contact.scss';

import Container from 'components/Container';
import Button from 'components/Button';

class Contact extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <Container>
        <h2>Contact Us</h2>
        <div className='contact-container-flex'>
          <div className='contact-map'>
            <div className='mapouter'>
              <div className='gmap_canvas'>
                <iframe
                  width='100%'
                  height='100%'
                  id='gmap_canvas'
                  src='https://maps.google.com/maps?q=289%20Tuam%20St&t=&z=17&ie=UTF8&iwloc=&output=embed'
                  frameBorder='0'
                  scrolling='no'
                  marginHeight='0'
                  marginWidth='0'
                ></iframe>
              </div>
            </div>
          </div>
          <div className='contact-description'>
            <h3>Information</h3>
            <p>
              <b>Address:</b> 177 High Street, Christchurch, New Zealand
            </p>
            <b>Working Hours:</b>
            <table className='contact-working-hours'>
              <tbody>
                <tr>
                  <td className='contact-default-trtd'>Monday</td>
                  <td>08:30 &ndash; 17:45</td>
                </tr>
                <tr>
                  <td className='contact-default-trtd'>Tuesday</td>
                  <td>08:30 &ndash; 17:45</td>
                </tr>
                <tr>
                  <td>Wednesday</td>
                  <td>08:30 &ndash; 17:45</td>
                </tr>
                <tr>
                  <td className='contact-default-trtd'>Thursday</td>
                  <td>08:30 &ndash; 17:45</td>
                </tr>
                <tr>
                  <td className='contact-default-trtd'>Friday</td>
                  <td>08:30 &ndash; 17:45</td>
                </tr>
                <tr>
                  <td className='contact-default-trtd'>Saturday</td>
                  <td>&mdash;</td>
                </tr>
                <tr>
                  <td className='contact-default-trtd'>Sunday</td>
                  <td>&mdash;</td>
                </tr>
              </tbody>
            </table>
            <p>
              <b>Email:</b>{' '}
              <a href='mailto:rubravi.official@gmail.com'>
                rubravi.official@gmail.com
              </a>
            </p>
            <p>
              <b>Phone:</b> <a href='tel:+64226263797'>+64 22626 3797</a>
            </p>
            <Button text='Send Enquiry' link='/products/order' />
          </div>
        </div>
      </Container>
    );
  }
}

export default Contact;
