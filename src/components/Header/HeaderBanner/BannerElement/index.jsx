import React from 'react';

import './BannerElement.scss';
import Button from 'components/Button';

class BannerElement extends React.Component {
  constructor() {
    super();
    this.state = {
      stylesApplied: false,
      visible: false,
      textBlockStyle: {}
    };
  }

  componentDidMount() {
    const stableStyles = {
      opacity: 1,
      marginLeft: 0,
      marginTop: 0
    };
    if (this.props.isActive) {
      this.setState({
        textBlockStyle: stableStyles
      });
    }
  }

  componentDidUpdate() {
    const stableStyles = {
      opacity: 1,
      marginLeft: 0,
      marginTop: 0
    };
    const currentlyHidden =
      Object.keys(this.state.textBlockStyle).length === 0 &&
      this.state.textBlockStyle.constructor === Object;

    if (!currentlyHidden && !this.props.isActive) {
      this.setState({
        textBlockStyle: {}
      });
    } else if (this.props.isActive && currentlyHidden) {
      // Timeout to display before animating
      setTimeout(() => {
        this.setState({
          textBlockStyle: stableStyles
        });
      }, 200);
    }
  }

  render() {
    return (
      <div className='header-banner' style={this.props.displayingStyles}>
        <div className='selling'>
          <div className='selling__description'>
            <div
              className='selling__text selling__animation_left'
              style={this.state.textBlockStyle}
            >
              <h2>{this.props.title}</h2>
            </div>
            <div
              className='selling__button selling__animation_left'
              style={this.state.textBlockStyle}
            >
              <Button
                text={this.props.buttonText}
                color={this.props.buttonColor}
                link={this.props.buttonLink}
              />
            </div>
          </div>
          <div
            className='selling__image selling__animation_top'
            style={this.state.textBlockStyle}
          >
            <img src={this.props.imageUrl} alt='' />
          </div>
        </div>
      </div>
    );
  }
}

export default BannerElement;
