import React from 'react';

import './HeaderBanner.scss';
import colors from 'styles/colors.scss';

import BannerElement from './BannerElement';

import ArrowLeft from 'resources/svg/arrow-left.svg';
import ArrowRight from 'resources/svg/arrow-right.svg';

import Image1 from 'resources/images/banner/private_docker.png';
import Image2 from 'resources/images/banner/high_performance.png';
import Image3 from 'resources/images/banner/scalable_apps.png';

class HeaderBanner extends React.Component {
  constructor() {
    super();
    this.state = {
      activeBannerIndex: 0,
      activeColor: '',
      bannerData: [
        {
          title: 'Private Docker Container Images',
          imageUrl: Image1,
          bgColor: colors.colorBlueBright,
          index: 0,
          isActive: true,
          buttonText: 'Get Now',
          buttonColor: 'neon',
          buttonLink: '/products',
          displayingStyles: {}
        },
        {
          title: 'High-Performance, Scalable VMs',
          buttonText: '',
          imageUrl: Image2,
          bgColor: colors.colorNeon,
          index: 1,
          isActive: false,
          buttonColor: 'blue',
          displayingStyles: { display: 'none' }
        },
        {
          title: 'Build Apps, Scale Automatically',
          imageUrl: Image3,
          bgColor: colors.colorPinkBright,
          index: 2,
          isActive: false,
          buttonText: 'Learn More',
          buttonColor: 'blue',
          buttonLink: '/products',
          displayingStyles: { display: 'none' }
        }
      ],
      banners: [],
      isSwitching: false
    };
    this.nextSlide = this.nextSlide.bind(this);
    this.prevSlide = this.prevSlide.bind(this);
    this.switchSlider = this.switchSlider.bind(this);
    this.bannerStatic = this.bannerStatic.bind(this);
    this.applyNewBanners = this.applyNewBanners.bind(this);
  }

  switchSlider(switching) {
    this.setState({
      isSwitching: switching
    });
  }

  applyNewBanners(nextIndex) {
    let newBanners = JSON.parse(JSON.stringify(this.state.bannerData));
    newBanners[nextIndex].displayingStyles = {};

    this.setState({
      activeBannerIndex: nextIndex,
      activeColor: this.state.bannerData[nextIndex].bgColor,
      bannerData: newBanners
    });

    let updatedBanners = JSON.parse(JSON.stringify(this.state.bannerData));
    updatedBanners.forEach(element => {
      element.displayingStyles = { display: 'none' };
    });
    updatedBanners[nextIndex].displayingStyles = {};

    // Timeout to wait until animation is finished
    setTimeout(() => {
      this.setState({
        bannerData: updatedBanners
      });
    }, 500);
  }

  nextSlide() {
    const nextIndex =
      this.state.activeBannerIndex + 1 < this.state.bannerData.length
        ? this.state.activeBannerIndex + 1
        : 0;
    this.applyNewBanners(nextIndex);
  }

  prevSlide() {
    const nextIndex =
      this.state.activeBannerIndex - 1 >= 0
        ? this.state.activeBannerIndex - 1
        : this.state.bannerData.length - 1;
    this.applyNewBanners(nextIndex);
  }

  componentDidMount() {
    this.setState({
      activeColor: this.state.bannerData[this.state.activeBannerIndex].bgColor
    });
  }

  bannerStatic() {
    if (!this.props.image) {
      return (
        <>
          <div
            className='header-banner-wrapper'
            style={{ backgroundColor: this.state.activeColor }}
          >
            {this.state.bannerData.map(item => {
              return (
                <BannerElement
                  key={item.index}
                  isActive={item.index === this.state.activeBannerIndex}
                  displayingStyles={item.displayingStyles}
                  title={item.title}
                  buttonText={item.buttonText}
                  imageUrl={item.imageUrl}
                  buttonColor={item.buttonColor}
                  buttonLink={item.buttonLink}
                />
              );
            })}
          </div>
          <div className='arrow banner__left-arrow'>
            <img src={ArrowLeft} alt='' onClick={this.prevSlide} />
          </div>
          <div className='arrow banner__right-arrow'>
            <img src={ArrowRight} alt='' onClick={this.nextSlide} />
          </div>
        </>
      );
    } else {
      return (
        <>
          <div
            className='header-banner-wrapper header-banner-static'
            style={{ backgroundImage: `url(${this.props.image})` }}
          >
            <BannerElement
              isActive={true}
              displayingStyles={{}}
              title={this.props.title}
              buttonText={this.props.buttonText}
              buttonColor={this.props.buttonColor}
              buttonLink={this.props.buttonLink}
            />
          </div>
        </>
      );
    }
  }

  render() {
    return <div className='banner'>{this.bannerStatic()}</div>;
  }
}

export default HeaderBanner;
