import React from 'react';
import { Link } from 'react-router-dom';

import './Menu.scss';
import media from 'styles/media.scss';
import Logo from 'resources/logo/logo_small.png';

import CloseSVG from 'resources/svg/close-btn.svg';

import ModalJoin from 'components/Modal/ModalJoin';
import { switchScrollBlocker } from 'utils/index.js';

class Menu extends React.Component {
  constructor() {
    super();
    this.state = {
      isOpenModal: false,
      isOpenMenu: false
    };
    this.switchOpenModalState = this.switchOpenModalState.bind(this);
    this.openMobileMenu = this.openMobileMenu.bind(this);
    this.resizeComponent = this.resizeComponent.bind(this);
    this.closeMobileMenu = this.closeMobileMenu.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeComponent);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeComponent);
  }

  resizeComponent() {
    if (window.innerWidth >= parseInt(media.tablet)) {
      this.closeMobileMenu();
    }
  }

  switchOpenModalState(e) {
    const isOpen = !this.state.isOpenModal;
    let clickedClasses = [];
    if (e.target.getAttribute('class') !== null)
      clickedClasses = e.target.getAttribute('class').split(' ');
    const closingElements = [
      'header-menu__button',
      'modal-wrapper',
      'modal-content-wrapper',
      'modal-heading__close-button',
      'modal-heading__close-button_image'
    ];
    for (let i = 0; i < clickedClasses.length; i++) {
      if (closingElements.indexOf(clickedClasses[i]) !== -1) {
        this.setState({ isOpenModal: isOpen });
        if (!this.state.isOpenMenu) switchScrollBlocker(isOpen);
        break;
      }
    }
  }

  closeMobileMenu() {
    if (this.state.isOpenMenu) {
      this.setState({ isOpenMenu: false });
    }
    switchScrollBlocker(false);
  }

  openMobileMenu() {
    const isOpen = !this.state.isOpenMenu;
    this.setState({ isOpenMenu: isOpen });
    switchScrollBlocker(isOpen);
  }

  openMenuStyles() {
    if (this.state.isOpenMenu) {
      return {
        webkitMask: `url(${CloseSVG}) no-repeat center`,
        mask: `url(${CloseSVG}) no-repeat center`,
        transform: 'rotate(180deg)',
        WebkitTransform: 'rotate(180deg)',
        MozTransform: 'rotate(180deg)',
        OTransform: 'rotate(180deg)',
        MsTransform: 'rotate(180deg)'
      };
    }
    return {};
  }

  render() {
    return (
      <div className='menu'>
        <div className='header-menu-wrapper'>
          <div className='header-menu'>
            <Link to='/'>
              <div className='header-menu__logo'>
                <img src={Logo} alt='' />
              </div>
            </Link>
            <div className='header-menu__elements'>
              <Link to='/products' className='header-menu__element'>
                <div className='header-menu__element-text'>Products</div>
              </Link>
              <Link to='/partners' className='header-menu__element'>
                <div className='header-menu__element-text'>Partners</div>
              </Link>
              <Link to='/about' className='header-menu__element'>
                <div className='header-menu__element-text'>About</div>
              </Link>
              <Link to='/contact' className='header-menu__element'>
                <div className='header-menu__element-text'>Contact Us</div>
              </Link>
            </div>
            <div className='header-menu__flex-empty' />
            <div className='header-menu__profile'>
              <div className='header-menu__profile-button'>
                <button
                  type='button'
                  className='header-menu__button header-menu__button_desktop'
                  onClick={e => this.switchOpenModalState(e)}
                >
                  Send Enquiry
                </button>
                <button
                  type='button'
                  className='header-menu__profile-button__button_mobile'
                  onClick={this.openMobileMenu}
                  style={this.openMenuStyles()}
                ></button>
              </div>
            </div>
            <div
              className='header-menu_mobile'
              style={!this.state.isOpenMenu ? { display: 'none' } : {}}
            >
              <ul className='header-menu__list'>
                <li className='header-menu__list-element'>
                  <Link to='/products' className='header-menu__element'>
                    Products
                  </Link>
                </li>
                <li className='header-menu__list-element'>
                  <Link to='/partners' className='header-menu__element'>
                    Partners
                  </Link>
                </li>
                <li className='header-menu__list-element'>
                  <Link to='/about' className='header-menu__element'>
                    About
                  </Link>
                </li>
                <li className='header-menu__list-element'>
                  <Link to='/contact' className='header-menu__element'>
                    Contact Us
                  </Link>
                </li>
                <button
                  type='button'
                  className='header-menu__button header-menu__button-mobile'
                  onClick={e => this.switchOpenModalState(e)}
                >
                  Send Enquiry
                </button>
              </ul>
            </div>
          </div>
        </div>
        <ModalJoin
          isOpen={this.state.isOpenModal}
          switchHandler={this.switchOpenModalState}
        />
      </div>
    );
  }
}

export default Menu;
