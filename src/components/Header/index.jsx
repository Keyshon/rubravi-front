import React from 'react';

import './Header.scss';

import HeaderBanner from './HeaderBanner';
import Menu from './Menu';

class Header extends React.Component {
  constructor() {
    super();
    this.renderBanner = this.renderBanner.bind(this);
  }

  renderBanner() {
    if (this.props.hasBanner)
      return (
        <HeaderBanner
          hasBanner
          image={this.props.image}
          title={this.props.title}
          buttonText={this.props.buttonText}
          buttonColor={this.props.buttonColor}
          buttonLink={this.props.buttonLink}
        />
      );
    else return null;
  }

  render() {
    return (
      <div className='header-wrapper'>
        <Menu />
        {this.renderBanner()}
      </div>
    );
  }
}

export default Header;
