import React from 'react';
import ReactDOM from 'react-dom';

class ProductBlock extends React.Component {
  constructor() {
    super();
    this.state = {
      visible: false
    };
    this.defaultPosition = this.defaultPosition.bind(this);
    this.getVisibility = this.getVisibility.bind(this);
  }

  componentDidMount() {
    const originRect = ReactDOM.findDOMNode(this).getBoundingClientRect();
    if (
      originRect.y < window.innerHeight - originRect.height ||
      navigator.userAgent.indexOf('Edge') !== -1 ||
      window.navigator.userAgent.indexOf('MSIE') !== -1 ||
      window.navigator.userAgent.indexOf('Trident/') !== -1
    )
      this.setState({
        visible: true
      });
    window.addEventListener('scroll', this.getVisibility);
  }

  getVisibility() {
    const rect = ReactDOM.findDOMNode(this).getBoundingClientRect();
    if (rect.y < window.innerHeight / 2) {
      this.setState({
        visible: true,
        activeStyle: {
          display: 'block'
        }
      });
      window.removeEventListener('scroll', this.getVisibility);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.getVisibility);
  }

  defaultPosition() {
    const customStyle = {
      opacity: 1,
      transform: 'translateX(0%)',
      WebkitTransform: 'translateY(0%)',
      MozTransform: 'translateY(0%)',
      OTransform: 'translateY(0%)',
      MsTransform: 'translateY(0%)'
    };
    return customStyle;
  }

  render() {
    return <div>Product Block</div>;
  }
}

export default ProductBlock;
