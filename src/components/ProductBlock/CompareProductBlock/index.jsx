import React from 'react';

import './CompareProductBlock.scss';
import media from 'styles/media.scss';

import Button from 'components/Button';

class CompareProductBlock extends React.Component {
  constructor() {
    super();
    this.state = {
      mobile: false
    };
    this.getProperFormat = this.getProperFormat.bind(this);
    this.resizeComponent = this.resizeComponent.bind(this);
  }

  componentDidMount() {
    this.resizeComponent();
    window.addEventListener('resize', this.resizeComponent);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeComponent);
  }

  getProperFormat(advantageType, advantageValue, isWide) {
    switch (advantageType) {
      case 'text':
        return advantageValue;
      case 'check':
        if (advantageValue) return '\u2714';
        else return '\u2716';
      case 'button':
        return (
          <Button
            text='Get Now'
            color='neon'
            link={advantageValue}
            double={isWide}
          />
        );
      default:
        return advantageValue;
    }
  }

  resizeComponent() {
    if (window.innerWidth >= parseInt(media.tablet)) {
      this.setState({
        mobile: false
      });
    } else {
      this.setState({
        mobile: true
      });
    }
  }

  render() {
    return (
      <div className='content-container-bg compare-product-bg'>
        <div className='content-container-wrapper'>
          <div className='content-container'>
            <div className='compare-product-title'>
              <h2>{this.props.title}</h2>
            </div>
            <div className='compare-product-table-wrapper'>
              {(() => {
                if (!this.state.mobile) {
                  return (
                    <table className='compare-product-table compare-product-table-bg'>
                      <tbody>
                        <tr>
                          <th></th>
                          {this.props.items.map(item => {
                            return (
                              <th
                                className='compare-product__property'
                                key={item.index}
                              >
                                <div className='compare-product-item-logo'>
                                  <img src={item.logo} alt='No img' />
                                </div>
                                <h5 className='compare-product__property'>
                                  {item.header}
                                </h5>
                              </th>
                            );
                          })}
                        </tr>
                        {this.props.advantages.map((advantage, index) => {
                          return (
                            <tr key={advantage.index}>
                              <td className='compre-product__position'>
                                {advantage.name}
                              </td>
                              {this.props.items.map(item => {
                                return (
                                  <td
                                    className='compare-product__property'
                                    key={item.index}
                                  >
                                    {this.getProperFormat(
                                      advantage.type,
                                      item.advantages[index].value
                                    )}
                                  </td>
                                );
                              })}
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  );
                } else {
                  return (
                    <div className='compare-product-table-bg'>
                      {this.props.items.map(item => {
                        return (
                          <div>
                            <h5>{item.header}</h5>
                            <table className='compare-product-table_mobile'>
                              {this.props.advantages.map((advantage, index) => {
                                if (advantage.type !== 'button') {
                                  return (
                                    <tr>
                                      <td>
                                        <b>{advantage.name}</b>
                                      </td>
                                      <td>
                                        {this.getProperFormat(
                                          advantage.type,
                                          item.advantages[index].value
                                        )}
                                      </td>
                                    </tr>
                                  );
                                } else {
                                  return (
                                    <tr>
                                      {this.getProperFormat(
                                        advantage.type,
                                        item.advantages[index].value,
                                        true
                                      )}
                                    </tr>
                                  );
                                }
                              })}
                            </table>
                          </div>
                        );
                      })}
                    </div>
                  );
                }
              })()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CompareProductBlock;
