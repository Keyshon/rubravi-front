import React from 'react';
import ProductBlock from 'components/ProductBlock';
import Tilt from 'react-tilt';

import './SplitProductBlock.scss';
import media from 'styles/media.scss';
import colors from 'styles/colors.scss';

import Button from 'components/Button';

class SplitProductBlock extends ProductBlock {
  constructor() {
    super();
    this.state = {
      mobile: false
    };
    this.tiltSettings = this.tiltSettings.bind(this);
    this.resizeComponent = this.resizeComponent.bind(this);
  }

  componentDidMount() {
    super.componentDidMount();
    this.resizeComponent();
    window.addEventListener('resize', this.resizeComponent);
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    window.removeEventListener('resize', this.resizeComponent);
  }

  resizeComponent() {
    if (window.innerWidth >= parseInt(media.tablet)) {
      this.setState({
        mobile: false
      });
    } else {
      this.setState({
        mobile: true
      });
    }
  }

  tiltSettings() {
    return {
      max: 15,
      scale: 1,
      speed: 50
    };
  }

  render() {
    return (
      <>
        <div className='content-container-bg split-bg'>
          <div className='content-container-wrapper'>
            <div className='content-container'>
              <div className='splitted-product-section'>
                <div className='product-section__image product-section__image_left'>
                  <Tilt
                    className='Tilt product-block-image-container product-block-image_floated'
                    options={this.tiltSettings()}
                  >
                    <img
                      src={this.props.imageLeft}
                      alt=''
                      style={this.state.visible ? this.defaultPosition() : {}}
                    />
                  </Tilt>
                </div>
                <div className='product-section__text_left'>
                  <h2>{this.props.titleLeft}</h2>
                  <p>{this.props.descriptionLeft}</p>
                  <p>{this.props.descriptionAdditionLeft}</p>
                  <div className='product-section__button'>
                    <Button
                      text={this.props.buttonTextLeft}
                      color={this.props.buttonColorLeft}
                      link={this.props.buttonLinkLeft}
                    />
                  </div>
                </div>
              </div>
              {(() => {
                if (!this.state.mobile) {
                  return (
                    <>
                      <div className='splitted-product-section splitted-last-section_mobile'>
                        <div className='product-section__image product-section__image_right'>
                          <Tilt
                            className='Tilt product-block-image-container'
                            options={this.tiltSettings()}
                          >
                            <img
                              src={this.props.imageRight}
                              alt=''
                              style={
                                this.state.visible ? this.defaultPosition() : {}
                              }
                            />
                          </Tilt>
                        </div>
                        <div className='product-section__text_right'>
                          <h2>{this.props.titleRight}</h2>
                          <p>{this.props.descriptionRight}</p>
                          <p>{this.props.descriptionAdditionRight}</p>
                          <div className='product-section__button'>
                            <Button
                              text={this.props.buttonTextRight}
                              color={this.props.buttonColorRight}
                              link={this.props.buttonLinkRight}
                            />
                          </div>
                        </div>
                      </div>
                      <br style={{ clear: 'both' }} />
                    </>
                  );
                } else return null;
              })()}
            </div>
          </div>
        </div>
        {(() => {
          if (!this.state.mobile) return null;
          else {
            return (
              <div
                className='content-container-bg split-bg'
                style={
                  this.props.buttonColorRight
                    ? { backgroundColor: colors.colorNeonBright }
                    : {}
                }
              >
                <div className='content-container-wrapper'>
                  <div className='content-container'>
                    <div className='splitted-product-section splitted-last-section_mobile'>
                      <div className='product-section__image product-section__image_right'>
                        <Tilt
                          className='Tilt product-block-image-container'
                          options={this.tiltSettings()}
                        >
                          <img
                            src={this.props.imageRight}
                            alt=''
                            style={
                              this.state.visible ? this.defaultPosition() : {}
                            }
                          />
                        </Tilt>
                      </div>
                      <div className='product-section__text_right'>
                        <h2>{this.props.titleRight}</h2>
                        <p>{this.props.descriptionRight}</p>
                        <p>{this.props.descriptionAdditionRight}</p>
                        <div className='product-section__button'>
                          <Button
                            text={this.props.buttonTextRight}
                            color={this.props.buttonColorRight}
                            link={this.props.buttonLinkRight}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          }
        })()}
      </>
    );
  }
}

export default SplitProductBlock;
