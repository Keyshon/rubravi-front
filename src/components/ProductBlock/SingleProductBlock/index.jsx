import React from 'react';
import ProductBlock from 'components/ProductBlock';

import 'styles/App.scss';
import media from 'styles/media.scss';
import './SingleProductBlock.scss';

import Button from 'components/Button';

class SingleProductBlock extends ProductBlock {
  constructor() {
    super();
    this.state = {
      mobile: false
    };
    this.placeImage = this.placeImage.bind(this);
    this.resizeComponent = this.resizeComponent.bind(this);
  }

  componentDidMount() {
    super.componentDidMount();
    this.resizeComponent();
    window.addEventListener('resize', this.resizeComponent);
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    window.removeEventListener('resize', this.resizeComponent);
  }

  resizeComponent() {
    if (window.innerWidth >= parseInt(media.tablet)) {
      this.setState({
        mobile: false
      });
    } else {
      this.setState({
        mobile: true
      });
    }
  }

  heightRestricted() {
    return this.props.restricted ? { maxHeight: '200px' } : {};
  }

  placeImage() {
    if (this.props.rightText) {
      return (
        <div className='flex-product-block'>
          <div className='product-image-block product-image-block_left'>
            <div className='product-image-block__absolute-animation'>
              <img
                src={this.props.image}
                alt=''
                style={
                  this.state.visible
                    ? { ...this.defaultPosition(), ...this.heightRestricted() }
                    : this.heightRestricted()
                }
              />
            </div>
          </div>
          <div className='product-text-block_right'>
            <h2>{this.props.header}</h2>
            <p>{this.props.description}</p>
            <p>{this.props.descriptionAddition}</p>
            <div className='product-list-section__button'>
              <Button
                text={this.props.buttonText}
                color={this.props.buttonColor}
                link={this.props.buttonLink}
              />
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className='flex-product-block'>
          {(() => {
            if (this.state.mobile)
              return (
                <div className='product-image-block product-image-block_right'>
                  <div className='product-image-block__absolute-animation'>
                    <img
                      src={this.props.image}
                      alt=''
                      style={
                        this.state.visible
                          ? {
                              ...this.defaultPosition(),
                              ...this.heightRestricted()
                            }
                          : this.heightRestricted()
                      }
                    />
                  </div>
                </div>
              );
            else return null;
          })()}
          <div className='product-text-block_left'>
            <h2>{this.props.header}</h2>
            <p>{this.props.description}</p>
            <p>{this.props.descriptionAddition}</p>
            <div className='product-list-section__button'>
              <Button
                text={this.props.buttonText}
                color={this.props.buttonColor}
                link={this.props.buttonLink}
              />
            </div>
          </div>
          {(() => {
            if (!this.state.mobile)
              return (
                <div className='product-image-block product-image-block_right'>
                  <div className='product-image-block__absolute-animation'>
                    <img
                      src={this.props.image}
                      alt=''
                      style={
                        this.state.visible
                          ? {
                              ...this.defaultPosition(),
                              ...this.heightRestricted()
                            }
                          : this.heightRestricted()
                      }
                    />
                  </div>
                </div>
              );
            else return null;
          })()}
        </div>
      );
    }
  }

  render() {
    return (
      <div className='content-container-bg'>
        <div className='content-container-wrapper'>
          <div className='content-container'>{this.placeImage()}</div>
        </div>
      </div>
    );
  }
}

export default SingleProductBlock;
