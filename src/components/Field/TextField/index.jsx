import React from 'react';

import './TextField.scss';
import colors from 'styles/colors.scss';

import Field from 'components/Field';

class TextField extends Field {
  constructor() {
    super();
    this.handleBlur = this.handleBlur.bind(this);
  }

  handleBlur() {
    this.deactivate();
    this.validate();
  }

  render() {
    return (
      <div className='field-section'>
        <input
          className='field'
          type={this.props.type}
          name={this.props.name}
          style={
            this.state.errorCode === '' ? {} : { borderColor: colors.colorRed }
          }
          onBlur={this.handleBlur}
          value={this.state.inputValue}
          onChange={evt => this.updateInputValue(evt)}
          onFocus={this.activate}
        />
        <div
          className='field-placeholder-text'
          style={this.placeholderStyles()}
        >
          {this.props.placeholder}
        </div>
        <div className='field-section__error-text'>{this.state.errorCode}</div>
      </div>
    );
  }
}

export default TextField;
