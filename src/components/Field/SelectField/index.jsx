import React from 'react';

import './SelectField.scss';
import colors from 'styles/colors.scss';

import Field from 'components/Field';

class SelectField extends Field {
  constructor() {
    super();
    this.state = {
      errorCode: '',
      inputValue: '',
      focused: false
    };
    this.defaultOption = this.defaultOption.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  updateInputValue(evt) {
    super.updateInputValue(evt);
    this.props.dispatch(evt.target.value);
  }

  componentDidMount() {
    this.setState({ inputValue: this.props.product });
  }

  componentDidUpdate() {}

  defaultOption() {
    if (this.state.inputValue === '') {
      return <option value='Default' defaultValue='selected'></option>;
    } else {
      return null;
    }
  }

  handleClick() {
    this.setState(prevState => {
      return {
        focused: !prevState.focused
      };
    });
  }

  handleBlur() {
    this.deactivate();
    this.setState({ focused: false });
  }

  validate() {
    if (this.state.inputValue === '') {
      const code = 'Please select an option';
      this.setState({ errorCode: code });
      return code;
    }

    return '';
  }

  render() {
    return (
      <div className='field-section'>
        <select
          className='field select'
          name={this.props.name}
          style={
            this.state.errorCode === '' ? {} : { borderColor: colors.colorRed }
          }
          onChange={evt => this.updateInputValue(evt)}
          onBlur={this.handleBlur}
          onClick={this.handleClick}
          onFocus={this.activate}
          value={this.props.state}
        >
          {this.defaultOption()}
          {this.props.ids.map((item, index) => {
            return (
              <option key={item} value={item}>
                {this.props.names[index]}
              </option>
            );
          })}
        </select>
        <div
          className='field-placeholder-text'
          style={this.placeholderStyles()}
        >
          {this.props.placeholder}
        </div>
        <div className='field-arrow'>
          <svg
            version='1.1'
            xmlns='http://www.w3.org/2000/svg'
            viewBox='0 0 129 129'
            enableBackground='new 0 0 129 129'
            transform={this.state.focused ? 'rotate(180)' : ''}
          >
            <g>
              <path d='m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z' />
            </g>
          </svg>
        </div>
        <div className='field-section__error-text'>{this.state.errorCode}</div>
      </div>
    );
  }
}

export default SelectField;
