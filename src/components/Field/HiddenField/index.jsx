import React from 'react';

class HiddenField extends React.Component {
  render() {
    return (
      <input type='hidden' name={this.props.name} value={this.props.value} />
    );
  }
}

export default HiddenField;
