import React from 'react';

import './PhoneField.scss';
import colors from 'styles/colors.scss';

import Field from 'components/Field';

import PhoneInput from 'react-phone-number-input';
import { isValidPhoneNumber } from 'react-phone-number-input';

class PhoneField extends Field {
  constructor() {
    super();
    this.handleBlur = this.handleBlur.bind(this);
  }

  placeholderStyles() {
    let inheritedStyles = super.placeholderStyles();
    if (this.state.active || this.state.inputValue !== '') {
      return {
        ...inheritedStyles,
        lineHeight: '30px'
      };
    }
    return {};
  }

  handleBlur() {
    this.deactivate();
    this.validate();
  }

  validate() {
    const val = this.state.inputValue;
    let code = '';

    if (this.props.checkEmpty && val === '') code = 'Enter some text';
    else if (!isValidPhoneNumber(val)) code = 'The phone format is invalid';

    this.setState({ errorCode: code });

    return code;
  }

  render() {
    return (
      <div className='field-section'>
        <PhoneInput
          style={
            this.state.errorCode === '' ? {} : { borderColor: colors.colorRed }
          }
          name={this.props.name}
          onChange={inputValue => this.setState({ inputValue })}
          onBlur={this.handleBlur}
          value={this.state.inputValue}
          onFocus={this.activate}
        />
        <div
          className='phone-field-placeholder-text field-placeholder-text'
          style={this.placeholderStyles()}
        >
          {this.props.placeholder}
        </div>
        <div className='field-section__error-text'>{this.state.errorCode}</div>
      </div>
    );
  }
}

export default PhoneField;
