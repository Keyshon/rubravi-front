import React from 'react';
import ReactDOM from 'react-dom';

import './../Field.scss';
import './SliderField.scss';
import colors from 'styles/colors.scss';

import Field from 'components/Field';
import HiddenField from 'components/Field/HiddenField';

class SliderField extends Field {
  constructor() {
    super();
    this.state = {
      errorCode: '',
      inputValue: 0,
      percent: 0,
      isMessage: false,
      isFetched: false
    };
    this.toggle = this.toggle.bind(this);
    this.untoggle = this.untoggle.bind(this);
    this.getCursor = this.getCursor.bind(this);
    this.calculateToggleStyles = this.calculateToggleStyles.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleFocus = this.handleFocus.bind(this);

    this.width = 0;
    this.xOffset = 0;
  }

  connectedHiddenField() {
    if (this.props.ids.length > 0) {
      return (
        <HiddenField name={this.props.aliasName} value={this.props.state} />
      );
    } else {
      return null;
    }
  }

  updateInputValue(evt) {
    let parsedString = parseInt(evt.target.value.replace(/\D/g, '')).toString();
    let isUpdated = false;

    if (isNaN(parseInt(parsedString)))
      this.setState({ inputValue: this.props.names[0] });
    else this.setState({ inputValue: parsedString });

    for (let i = 0; i < this.props.ids.length; i++) {
      if (this.props.names[i] === parsedString) {
        if (this.props.ids[i] === undefined) this.props.dispatch(0);
        else this.props.dispatch(this.props.ids[i]);
        this.setState({
          percent: parseInt((i / (this.props.ids.length - 2)) * 100)
        });
        this.validate(parsedString);
        isUpdated = true;
        break;
      }
    }

    if (!isUpdated) {
      this.props.dispatch(this.props.ids[this.props.ids.length - 1]);
      this.props.dispatchTerm(this.state.inputValue);
      this.validate(parsedString);
    }
  }

  componentWillReceiveProps() {
    if (!this.state.isFetched) {
      if (this.props.state === '' || this.props.state === undefined) {
        this.setState({
          inputValue: this.props.names[0],
          isFetched: true
        });
      } else {
        this.setState({
          inputValue: this.props.stateTerm
        });
        for (let i = 0; i < this.props.ids.length - 1; i++) {
          if (this.props.ids[i] === this.props.state) {
            this.setState({
              percent: parseInt((i / (this.props.ids.length - 2)) * 100),
              isFetched: true
            });
            this.validate(this.props.names[i]);
            break;
          }
        }
      }
      this.setState({ isFetched: true });
      this.validate();
    }
  }

  componentWillUnmount() {
    this.untoggle();
  }

  getClosestValue(percent) {
    const maxVals = this.props.names.length - 2; // Minus the customizable (last) one
    const normalized = Math.floor(percent / (100 / maxVals));
    return this.props.names[normalized];
  }

  getCursor(e) {
    let position = e.pageX - this.xOffset;
    if (position <= 0) position = 0;
    else if (position > this.width) position = this.width;

    const percent = (position / this.width) * 100;

    const closest = this.getClosestValue(percent);

    this.setState({ percent: percent });

    if (closest !== this.state.inputValue) {
      this.setState({ inputValue: closest });

      for (let i = 0; i < this.props.ids.length; i++) {
        if (this.props.names[i] === closest) {
          this.props.dispatch(this.props.ids[i]);
          this.props.dispatchTerm(this.props.names[i]);
          break;
        } else {
          this.props.dispatch(this.props.ids[this.props.ids.length - 1]);
          this.props.dispatchTerm(this.props.names[this.props.ids.length - 1]);
        }
      }
      this.validate(closest);
    }
  }

  untoggle() {
    window.removeEventListener('mouseup', this.untoggle);
    window.removeEventListener('mousemove', this.getCursor);
    document.body.style.userSelect = 'auto';
  }

  toggle(e) {
    const rect = ReactDOM.findDOMNode(this).getBoundingClientRect();

    this.width = rect.width - 30;
    this.xOffset = rect.x + 10;

    document.body.style.userSelect = 'none';

    window.addEventListener('mouseup', this.untoggle);
    window.addEventListener('mousemove', this.getCursor);
  }

  validate(newVal = this.state.inputValue) {
    const code = super.validate();

    if (code === '' && parseInt(newVal) !== 0) {
      if (this.props.names.indexOf(parseInt(newVal)) === -1) {
        this.setState({
          errorCode: 'You chose a customizable plan.',
          isMessage: true
        });
      } else {
        this.setState({
          errorCode: ''
        });
      }
    } else if (parseInt(this.state.inputValue) === 0) {
      this.setState({
        errorCode: 'Unlimited free plan.',
        isMessage: true
      });
    } else {
      this.setState({
        isMessage: false
      });
    }

    return code;
  }

  calculateToggleStyles() {
    if (this.width > 0)
      return { left: (this.state.percent * this.width) / 100 };
    else {
      if (this.state.percent !== 100) return { left: `${this.state.percent}%` };
      else return { left: `calc(${this.state.percent}% - 18px)` };
    }
  }

  handleBlur(evt) {
    this.deactivate();
    this.updateInputValue(evt);
  }

  handleFocus(evt) {
    this.activate();
    this.setState({ inputValue: evt.target.value });
  }

  render() {
    return (
      <div className='field-section'>
        <input
          className='field'
          type={this.props.type}
          name={this.props.name}
          style={
            this.state.errorCode !== '' && !this.state.isMessage
              ? { borderColor: colors.colorRed }
              : {}
          }
          onBlur={evt => this.handleBlur(evt)}
          value={this.state.inputValue}
          onChange={evt => this.setState({ inputValue: evt.target.value })}
        />
        <div
          className='field-placeholder-text'
          style={this.placeholderStyles()}
        >
          {this.props.placeholder}
        </div>
        <div className='field-description-unit-text'>
          {this.props.unit + (() => (this.state.inputValue == 1 ? '' : 's'))()}
        </div>
        {(() => {
          if (
            window.navigator.userAgent.indexOf('MSIE') === -1 &&
            window.navigator.userAgent.indexOf('Trident/') === -1
          ) {
            return (
              <div className='field-slider'>
                <div
                  className='field-slider__toggle'
                  onMouseDown={e => this.toggle(e)}
                  style={this.calculateToggleStyles()}
                />
                <div
                  className='field-slider__progress'
                  style={{
                    clipPath: `polygon(0% 0%, 0% 100%, ${this.state.percent}% 100%, ${this.state.percent}% 0%)`
                  }}
                />
              </div>
            );
          }
        })()}
        <div
          className='field-section__error-text'
          style={this.state.isMessage ? { color: colors.colorBlack } : {}}
        >
          {' '}
          {this.state.errorCode}{' '}
        </div>
        {this.connectedHiddenField()}
      </div>
    );
  }
}

export default SliderField;
