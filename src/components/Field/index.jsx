import React from 'react';

import './Field.scss';
import colors from 'styles/colors.scss';

class Field extends React.Component {
  constructor() {
    super();
    this.state = {
      errorCode: '',
      inputValue: '',
      active: false
    };
    this.validate = this.validate.bind(this);
    this.placeholderStyles = this.placeholderStyles.bind(this);
    this.activate = this.activate.bind(this);
    this.deactivate = this.deactivate.bind(this);
  }

  placeholderStyles() {
    if (this.state.active || this.state.inputValue !== '') {
      return {
        fontSize: '12px',
        color: colors.colorNeonAccent,
        lineHeight: '30px',
        paddingLeft: '17px'
      };
    }
    return {};
  }

  activate() {
    this.setState({ active: true });
  }

  deactivate() {
    this.setState({ active: false });
  }

  updateInputValue(evt) {
    this.setState({ inputValue: evt.target.value });
  }

  validate() {
    const val = this.state.inputValue;
    let code = '';

    if (this.props.checkEmpty && val === '') code = 'Enter some text';
    else {
      if (
        this.props.minLength &&
        val !== undefined &&
        val.length < this.props.minLength
      )
        code = `Minimum ${this.props.minLength} characters`;
      if (
        this.props.maxLength &&
        val !== undefined &&
        val.length > this.props.maxLength
      )
        code = `Maximum ${this.props.maxLength} characters`;
      if (val === undefined) code = 'Enter some text';
    }

    this.setState({ errorCode: code });

    return code;
  }

  render() {
    return (
      <div className='field-section'>
        Please, refer to TextField or other component
      </div>
    );
  }
}

export default Field;
