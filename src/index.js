import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './styles/App.scss';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import 'isomorphic-fetch';

import persistedStore from 'store';

ReactDOM.render(
  <Provider store={persistedStore().store}>
    <PersistGate loading={null} persistor={persistedStore().persistor}>
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById('root')
);
