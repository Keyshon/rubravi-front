import BACKEND_API from 'api/config.js';

const getProducts = () => {
  return fetch(`http://${BACKEND_API}/api/products`, {
    method: 'GET',
    headers: {
      Accept: 'application/json'
    }
  });
};

const getProduct = id => {
  return fetch(`http://${BACKEND_API}/api/products/${id}`, {
    method: 'GET',
    headers: {
      Accept: 'application/json'
    }
  });
};

export { getProducts, getProduct };
