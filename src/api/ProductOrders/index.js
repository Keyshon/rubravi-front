import BACKEND_API from 'api/config.js';

const postOrder = (
  productType,
  userName,
  userPhone,
  userEmail,
  subscriptionType,
  subscriptionTerm
) => {
  return fetch(`http://${BACKEND_API}/api/orders`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      productType,
      userName,
      userPhone,
      userEmail,
      subscriptionType,
      subscriptionTerm
    })
  });
};

export default postOrder;
