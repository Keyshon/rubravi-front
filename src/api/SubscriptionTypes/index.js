import BACKEND_API from 'api/config.js';

const getSubscription = () => {
  return fetch(`http://${BACKEND_API}/api/subscriptiontypes`, {
    method: 'GET',
    headers: {
      Accept: 'application/json'
    }
  });
};

export default getSubscription;
