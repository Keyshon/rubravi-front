import React from 'react';
import { Helmet } from 'react-helmet';

import './pages.scss';

import Header from 'components/Header/index';
import Footer from 'components/Footer';

import Container from 'components/Container';

class PrivacyPolicy extends React.Component {
  constructor() {
    super();
  }
  render() {
    return (
      <>
        <Helmet>
          <title>RuBraVi - Privacy Policy</title>
        </Helmet>
        <Header />
        <div className='content-wrapper'>
          <Container>
            <h2>Privacy Policy</h2>
            <p>
              The Privacy Policy describes our practices concerning the
              information collected by RuBraVi, Inc. (“we” or “us”) through the
              use of rubravi.com (https://rubravi.com/) (the “Web Site”) and the
              rubravi.com mobile application (the “Application”, and together
              with the Web Site, the “Services”). We recognize the importance of
              maintaining your privacy. We value our relationship with you and
              we appreciate the trust you show in providing information to us.
              Explaining how we collect, use and protect customer information
              is, therefore, of utmost importance to us.
            </p>
            <p>
              This Privacy Policy is designed to tell you how we collect and use
              personal information (as defined below). Please read this
              statement before using the Services or submitting any personal
              information to us.
            </p>
            <h3>Information We Collect</h3>
            <h4>Information You Provide to Us</h4>
            <p>
              When you visit our Services, we automatically receive and store
              any information you enter on our Services. You have the option not
              to provide certain information; however, withholding information
              may prevent you from being able to take full advantage of some of
              our Services’ features.
            </p>
            <p>
              You provide information to us when you: browse our Services;
              provide information in your account; communicate with us by phone,
              chat, text message or email; complete a questionnaire; or engage
              in other interactions with us.
            </p>
            <p>
              Through these interactions, you may provide information including,
              but not limited to: name, address, e-mail address, phone numbers,
              social security number, ID, bank statements, letters of
              employment, employment contracts, pay stubs, letters of
              acceptance, letters of recommendation, references from landlords
              or roommates, visa information, transactional information (based
              on your activities on the Services and content you generate or
              that relates to your account), community discussions, chats,
              dispute resolution, and the contents of any correspondence sent to
              us.
            </p>
            If you make a payment on the Services, your payment-related
            information, such as credit card or other financial information, may
            be collected by a third party payment processor on our behalf.
            <h3>
              Information We Collect Automatically When You Use Our Services
            </h3>
            <p>
              We also automatically collect certain types of information about
              you or your device when you interact with our Services.
            </p>
            <p>
              Cookie. We and third party partners collect information using
              cookie, pixel tags, or similar technologies. Our third party
              partners, such as analytics and advertising partners, may use
              these technologies to collect information about your online
              activities over time and across different services. Cookie are
              small text files containing a string of alphanumeric characters.
              When you visit or return to our Services, we read the cookie to
              identify you as one of our customers and help you recall orders or
              preferences you have selected. Cookie also enables us to recall
              your past activities, post your account data, send e-mail to you,
              and tailor the Services’ elements and information to you. We also
              use this information to analyze trends, administer the Services,
              track users’ movements around the Services, gather demographic
              information about our user base, and conduct our marketing
              campaigns. Most web browsers accept cookie by default, but allow
              users to reject cookie by changing their browser preferences. If
              you have set up your browser to reject cookie, some aspects of our
              Services will not work properly. For more information about our
              cookie practices, please visit our cookie policy.
            </p>
            <p>
              Usage and Device Information. To help us understand how you use
              our Services and to help us improve them, we automatically receive
              information about your interactions with our Services, and the
              device and software you use to access them. For example, we
              collect the full Uniform Resource Locator (URL) click stream to,
              through and from our Services, including date and time; cookie
              number; and the phone number you used to call our +64 22626 3797
              number.
            </p>
            <p>
              We also collect IP address, browser type and version, operating
              system and platform, statistics on page views, traffic to and from
              the Services, ad data, standard web log information, session
              information, including page response times, download errors,
              length of visits to certain pages, page interaction information
              (such as scrolling, clicks, and mouse-overs), and methods used to
              browse away from the page.
            </p>
            <p>
              Information We Receive from Third Parties We might also receive
              information about you from other sources. Examples of this
              information may include: information from social media services
              that we have integrated into our Services (e.g., Facebook);
              page-view information from merchants with which we operate
              co-branded businesses or for which we provide technical,
              fulfillment, advertising, or other services; and information from
              partners like Zillow and Trulia who share your search term and
              search result information with us.
            </p>
            <p>
              We also receive credit history information from credit bureaus and
              third-party credit card fraud detection services, which we use to
              help prevent and detect fraud. Also, if the information you
              provide cannot be verified, we may ask you to send us additional
              information (such as your driver’s license, credit card statement,
              and/or a recent utility bill or other information confirming your
              address), or to answer additional questions online to help verify
              your information.
            </p>
            <h3>How We Use the Information We Collect</h3>
            <p>We use the information we collect:</p>
            <ul>
              <li>To provide, maintain, improve, and enhance our Services;</li>
              <li>
                To personalize your experience on our Services such as by
                providing tailored content and recommendations;
              </li>
              <li>
                To understand and analyze how you use our Services and develop
                new products, services, features, and functionality;
              </li>
              <li>
                To communicate with you, provide you with updates and other
                information relating to our Services, provide information that
                you request, respond to comments and questions, and otherwise
                provide customer support;
              </li>
              <li>
                For marketing and advertising purposes, such as developing and
                providing promotional and advertising materials that may be
                relevant, valuable or otherwise of interest to you;
              </li>
              <li>
                To generate anonymized, aggregate data containing only
                de-identified, non-personal information that we may use to
                publish reports or conduct internal research;
              </li>
              <li>To facilitate transactions and payments;</li>
              <li>
                To find and prevent fraud, and respond to trust and safety
                issues that may arise;
              </li>
              <li>
                For compliance purposes, including enforcing our terms of
                service or other legal rights, or as may be required by
                applicable laws and regulations or requested by any judicial
                process or governmental agency;
              </li>
              <li>
                For other purposes for which we provide specific notice at the
                time the information is collected.
              </li>
              <li>How We Share the Information We Collect</li>
              <li>
                Affiliates. We may disclose the information we collect from you
                to our affiliates or subsidiaries and these entities may use the
                information for marketing purposes; however, if we do so, the
                use and disclosure of such information will be subject to this
                Privacy Policy.
              </li>
            </ul>
            <p>
              Vendors and Service Providers. We may share any information we
              receive with vendors and service providers retained in connection
              with the provision of our Services.
            </p>
            <p>
              Analytics Partners. We use analytics services such as Google
              Analytics to collect and process certain analytics data. These
              services may also collect information about your use of other
              websites, apps, and online resources. You can learn about Google’s
              practices by going to
              https://www.google.com/policies/privacy/partners/, and opt-out of
              them by downloading the Google Analytics opt-out browser add-on,
              available at https://tools.google.com/dlpage/gaoptout.
            </p>
            <p>
              Third Party Advertisers. We use third-party advertising companies
              to e-mail or serve ads on our behalf. We may also serve ads or
              send e-mail which incorporates third party cookie or action tags.
              These cookie or action tags may track your response to e-mail or
              advertisements and measure their effectiveness or award incentives
              or points to their members who respond to their advertisements.
              Some of our advertising partners are members of the Network
              Advertising Initiative
              (http://optout.networkadvertising.org/?c=1#!/ ) or the Digital
              Advertising Alliance (http://optout.aboutads.info/?c=2&lang=EN ).
              If you do not wish to receive personalized ads, please visit their
              opt-out pages to learn about how you may opt out of receiving
              web-based personalized ads from member companies. You can access
              any settings offered by your mobile operating system to limit ad
              tracking, or you can install the AppChoices mobile app to learn
              more about how you may opt out of personalized ads in mobile apps.
            </p>
            <p>
              Sweepstakes, Contests and Surveys. From time-to-time we may
              provide you the opportunity to participate in sweepstakes,
              contests or surveys on our Services. If you participate, we will
              request certain information from you. Participation in
              sweepstakes, contests or surveys is completely voluntary and you
              therefore have a choice whether or not to disclose this
              information. By entering a sweepstakes or contest, you will be
              agreeing to the official rules governing the sweepstakes or
              contest and we may post on our Services the names of the
              sweepstakes or contest winners.
            </p>
            <p>
              Business Transfers. If we are acquired by or merged with another
              company, if substantially all of our assets are transferred to
              another company, or as part of a bankruptcy proceeding, we may
              transfer the information we have collected from you to the
              acquiring company.
            </p>
          </Container>
          <Footer />
        </div>
      </>
    );
  }
}

export default PrivacyPolicy;
