import React from 'react';
import { Helmet } from 'react-helmet';

import './pages.scss';

import Header from 'components/Header/index';
import Footer from 'components/Footer';

import Container from 'components/Container';
import SingleProductBlock from 'components/ProductBlock/SingleProductBlock';
import AdvantageList from 'components/List/AdvantageList';
import Hint from 'components/Hint';

import CocaCola from 'resources/images/partners/cocacola.png';
import Google from 'resources/images/partners/google.png';
import NZ from 'resources/images/partners/nz.png';
import Chch from 'resources/images/partners/christchurch.jpg';
import Handshake from 'resources/svg/partners/handshake.svg';
import Highfive from 'resources/svg/partners/highfive.svg';
import Target from 'resources/svg/partners/target.svg';

class Partners extends React.Component {
  constructor() {
    super();
  }
  render() {
    return (
      <>
        <Helmet>
          <title>RuBraVi - Partners</title>
        </Helmet>
        <Header />
        <div className='content-wrapper'>
          <Container short>
            <h2>Our partners</h2>
          </Container>
          <SingleProductBlock
            header='Coca-cola'
            description='The Coca-Cola Company, American corporation founded in 1892 
            and today engaged primarily in the manufacture and sale of syrup and 
            concentrate for Coca-Cola, a sweetened carbonated beverage that is a 
            cultural institution in the United States and a global symbol of American tastes.'
            image={CocaCola}
            restricted
          />
          <SingleProductBlock
            header='Google'
            description="Google is a multinational, publicly-traded organization built around 
            the company's hugely popular search engine. Google's other enterprises include 
            Internet analytics, cloud computing, advertising technologies, and Web app, browser 
            and operating system development."
            image={Google}
            restricted
          />
          <SingleProductBlock
            header='New Zealand Government'
            description='Govt.nz is for people who need to interact with the NZ government in 
            the course of their day-to-day lives including NZ citizens and residents, or people 
            who want to move to or visit NZ.'
            descriptionAddition='Information on Govt.nz aims to improve co-operation and 
            partnership across agencies so that users have a simpler and more trustworthy 
            experience when they’re interacting with government.'
            image={NZ}
            restricted
          />
          <AdvantageList
            advantage
            darken={60}
            background={Chch}
            items={[
              {
                key: 0,
                logo: Handshake,
                logoColor: {},
                title: 'Partnership',
                text:
                  'RuBraVi staff and management team includes a wealth of highly experienced individuals with a combined experience in business, administration, customer service and human resources spanning decades'
              },
              {
                key: 1,
                logo: Highfive,
                logoColor: {},
                title: 'Achieve',
                text:
                  "RuBraVi company is Oceania's leading and most trusted provider of employee assistance counselling services, training services and total wellness solutions"
              },
              {
                key: 2,
                logo: Target,
                logoColor: {},
                title: 'Support',
                text:
                  'Behind us is an unfaltering team and our loyal clients - helping us to help you and your colleagues be happier, healthier and more productive at work and home'
              }
            ]}
          />
          <Container>
            <h2>Be in contact</h2>
            <p>
              We are happy to see you as out partner. Our company provides the
              most secure networking solutions all around the world. We are
              ready to have a conversation to provide protection for the users,
              resources, and environment with defense-in-depth network security.
              As well as that, RuBraVi thinks of ensuring reliability with
              industry-leading SLAs and reduced lag and innovating on RuBraVi
              Cloud using protocols.
            </p>
            <p>
              Your business's most challenging security scenarios are protected
              by the same secure-by-design infrastructure, global network, and
              built-in safeguards that RuBraVi uses to protect information,
              identities, applications, and devices. Our managed, cloud-native
              solution means your developers can write an application once, then
              run it on-premises, on{' '}
              <Hint hintText='RuBraVi Cloud Private'>RCP</Hint>, or on other
              clouds with no change in infrastructure.
            </p>
          </Container>
          <Footer />
        </div>
      </>
    );
  }
}

export default Partners;
