import React from 'react';
import { Helmet } from 'react-helmet';

import './pages.scss';

import Header from 'components/Header/index';
import Footer from 'components/Footer';

import Container from 'components/Container';
import Button from 'components/Button';

import { fillHeightContainer } from 'utils';
class Page404 extends React.Component {
  constructor() {
    super();
    this.state = {
      height: 0
    };
    this.updateHeight = this.updateHeight.bind(this);
  }

  componentDidMount() {
    this.updateHeight();
    window.addEventListener('resize', this.updateHeight);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateHeight);
  }

  updateHeight() {
    this.setState({
      height: fillHeightContainer()
    });
  }

  render() {
    return (
      <>
        <Helmet>
          <title>RuBraVi - Not Found</title>
        </Helmet>
        <Header />
        <div className='content-wrapper'>
          <Container>
            <div
              className='page-notfound-flex'
              style={{ height: this.state.height }}
            >
              <div className='page-notfound-logo'></div>
              <div className='page-notfound-content'>
                <h2>Oops. Something went Wrong.</h2>
                Please, start again from the Home page.
                <div className='page-notfound-button'>
                  <Button text='Back to Home' color='blue' link='/' />
                </div>
              </div>
            </div>
          </Container>
          <Footer isFixed={false} />
        </div>
      </>
    );
  }
}

export default Page404;
