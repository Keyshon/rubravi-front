import React from 'react';
import { Helmet } from 'react-helmet';

import Header from 'components/Header/index';
import Footer from 'components/Footer';

import SingleProductBlock from 'components/ProductBlock/SingleProductBlock';
import SplitProductBlock from 'components/ProductBlock/SplitProductBlock';
import CompareProductBlock from 'components/ProductBlock/CompareProductBlock';

import ImageTemp1 from 'resources/images/products/cloud-armor.png';
import ImageTemp2 from 'resources/images/products/cloud-nat.png';
import ImageTemp3 from 'resources/images/products/vpc-service-control.png';
import ImageTemp4 from 'resources/images/products/network-telemetry.png';

import Image0 from 'resources/images/compare/battery-low.png';
import Image1 from 'resources/images/compare/battery-medium.png';
import Image2 from 'resources/images/compare/battery-high.png';
import Image3 from 'resources/images/compare/battery-full.png';

const Products = () => {
  return (
    <>
      <Helmet>
        <title>RuBraVi - Products</title>
      </Helmet>
      <Header />
      <div className='content-wrapper'>
        <CompareProductBlock
          title='Cloud networking products'
          advantages={[
            {
              name: 'OS',
              type: 'text',
              index: 0
            },
            {
              name: 'Virtual CPU',
              type: 'text',
              index: 1
            },
            {
              name: 'Memory',
              type: 'text',
              index: 2
            },
            {
              name: 'Support Term',
              type: 'text',
              index: 3
            },
            {
              name: 'Price',
              type: 'text',
              index: 4
            },
            {
              name: 'Non-preemptible',
              type: 'check',
              index: 5
            },
            {
              name: '',
              type: 'button',
              index: 6
            }
          ]}
          items={[
            {
              index: 0,
              header: 'Free',
              logo: Image0,
              advantages: [
                {
                  value: 'Ubuntu'
                },
                {
                  value: '2'
                },
                {
                  value: '4 GB'
                },
                {
                  value: '-'
                },
                {
                  value: 'Free'
                },
                {
                  value: false
                },
                {
                  value: '/products/order'
                }
              ]
            },
            {
              index: 1,
              header: 'Standard',
              logo: Image1,
              advantages: [
                {
                  value: 'Ubuntu, Windows'
                },
                {
                  value: '2'
                },
                {
                  value: '8 GB'
                },
                {
                  value: '3 months'
                },
                {
                  value: '65$ / month'
                },
                {
                  value: false
                },
                {
                  value: '/products/order'
                }
              ]
            },
            {
              index: 2,
              header: 'Pro',
              logo: Image2,
              advantages: [
                {
                  value: 'Ubuntu, Windows'
                },
                {
                  value: '4'
                },
                {
                  value: '16 GB'
                },
                {
                  value: '12 months'
                },
                {
                  value: '90$ / month'
                },
                {
                  value: true
                },
                {
                  value: '/products/order'
                }
              ]
            },
            {
              index: 3,
              header: 'Ultimate',
              logo: Image3,
              advantages: [
                {
                  value: 'Ubuntu, Windows'
                },
                {
                  value: '8'
                },
                {
                  value: '32 GB'
                },
                {
                  value: 'Unlimited'
                },
                {
                  value: '130$ / month'
                },
                {
                  value: true
                },
                {
                  value: '/products/order'
                }
              ]
            }
          ]}
        />
        <SingleProductBlock
          header='Cloud Armor'
          description="RuBraVi Cloud Armor works with an HTTP(S) load balancer to provide built-in 
          defenses against infrastructure DDoS attacks. RuBraVi Cloud Armor benefits from more than 
          a decade of experience protecting the world's largest internet properties like Google Search, 
          Gmail, and YouTube."
          image={ImageTemp1}
          buttonText='Order Now'
          buttonLink='/products/order'
          buttonColor='neon'
        />
        <SingleProductBlock
          header='Cloud NAT'
          description="Cloud NAT, RuBraVi Cloud's managed network address translation service, enables 
          you to provision application instances without public IP addresses while also allowing access 
          to the internet in a controlled and efficient manner. Outside resources cannot directly access 
          any of the private instances behind the Cloud NAT gateway, helping to keep your RuBraVi Cloud 
          VPCs isolated and secure."
          image={ImageTemp2}
          buttonText='Order Now'
          buttonColor='neon'
          buttonLink='/products/order'
          rightText
        />

        <SplitProductBlock
          imageLeft={ImageTemp3}
          titleLeft='VPC Service Controls'
          descriptionLeft="VPC Service Controls allows users to define a security perimeter around 
          RuBraVi Cloud Platform resources like Cloud Storage buckets, Bigtable instances, and BigQuery 
          datasets to constrain data within a VPC and help mitigate data exfiltration risks. VPC 
          Service Controls enables enterprises to keep their sensitive data private while leveraging 
          RuBraVi Cloud Platform's fully managed storage and data processing capabilities."
          buttonTextLeft='Get Service Controls'
          buttonColorLeft='neon'
          buttonLinkLeft='/products/order'
          imageRight={ImageTemp4}
          titleRight='Network Telemetry'
          descriptionRight='Keep services secure with in-depth network telemetry networking services.'
          descriptionAdditionRight='Identify traffic and access patterns that may impose security or operational
          risks to your organization in near real time. Network Telemetry provides both network and
          security operations with in-depth, responsive VPC flow logs for RuBraVi Cloud Platform.'
          buttonTextRight='Get Network Telemetry'
          buttonColorRight='lightBlue'
          buttonLinkRight='/products/order'
        />
        <Footer />
      </div>
    </>
  );
};

export default Products;
