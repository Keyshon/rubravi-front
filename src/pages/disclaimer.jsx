import React from 'react';
import { Helmet } from 'react-helmet';

import './pages.scss';

import Header from 'components/Header/index';
import Footer from 'components/Footer';

import Container from 'components/Container';

class Disclaimer extends React.Component {
  constructor() {
    super();
  }
  render() {
    return (
      <>
        <Helmet>
          <title>RuBraVi - Disclaimer</title>
        </Helmet>
        <Header />
        <div className='content-wrapper'>
          <Container>
            <h2>Disclaimer</h2>
            <p>
              RuBraVi, Inc. ("us", "we", or "our") uses cookie on the
              https://rubravi.com/ website and the RuBraVi App mobile
              application (the "Service"). By using the Service, you consent to
              the use of cookie.
            </p>
            <p>
              Our Cookie Policy explains what cookie are, how we use cookie, how
              third-parties we may partner with may use cookie on the Service,
              your choices regarding cookie and further information about
              cookie.
            </p>
            <h3>What are cookie</h3>
            <p>
              cookie are small pieces of text sent to your web browser by a
              website you visit. A cookie file is stored in your web browser and
              allows the Service or a third-party to recognize you and make your
              next visit easier and the Service more useful to you.
            </p>
            <p>
              cookie can be "persistent" or "session" cookie. Persistent cookie
              remain on your personal computer or mobile device when you go
              offline, while session cookie are deleted as soon as you close
              your web browser.
            </p>
            <h3>How RuBraVi, Inc. uses cookie</h3>
            <p>
              When you use and access the Service, we may place a number of
              cookie files in your web browser. We use cookie for the following
              purposes: To enable certain functions of the Service To provide
              analytics To store your preferences We use both session and
              persistent cookie on the Service and we use different types of
              cookie to run the Service: Essential cookie. We may use cookie to
              remember information that changes the way the Service behaves or
              looks, such as a user's language preference on the Service.
              Accounts-related cookie. We may use accounts-related cookie to
              authenticate users and prevent fraudulent use of user accounts. We
              may use these cookie to remember information that changes the way
              the Service behaves or looks, such as the "remember me"
              functionality. Analytics cookie. We may use analytics cookie to
              track information how the Service is used so that we can make
              improvements. We may also use analytics cookie to test new
              advertisements, pages, features or new functionality of the
              Service to see how our users react to theme Third-party cookie In
              addition to our own cookie, we may also use various third-parties
              cookie to report usage statistics of the Service, deliver
              advertisements on and through the Service, and so on.
            </p>
            <h3>What are your choices regarding cookie</h3>
            <p>
              If you'd like to delete cookie or instruct your web browser to
              delete or refuse cookie, please visit the help pages of your web
              browser.
            </p>
            <p>
              Please note, however, that if you delete cookie or refuse to
              accept them, you might not be able to use all of the features we
              offer, you may not be able to store your preferences, and some of
              our pages might not display properly.
              <ul>
                <li>
                  For the Chrome web browser, please visit this page from
                  Google: https://support.google.com/accounts/answer/32050
                </li>
                <li>
                  For the Internet Explorer web browser, please visit this page
                  from Microsoft: http://support.microsoft.com/kb/278835
                </li>
                <li>
                  For the Firefox web browser, please visit this page from
                  Mozilla:https://support.mozilla.org/en-US/kb/delete-cookie-remove-info-websites-stored
                </li>
                <li>
                  For the Safari web browser, please visit this page from Apple:
                  https://support.apple.com/guide/safari/manage-cookie-and-website-data-sfri11471/mac
                </li>
                <li>
                  For any other web browser, please visit your web browser's
                  official web pages
                </li>
              </ul>
            </p>
          </Container>
          <Footer />
        </div>
      </>
    );
  }
}

export default Disclaimer;
