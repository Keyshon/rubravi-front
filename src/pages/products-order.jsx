import React from 'react';
import { Helmet } from 'react-helmet';

import Header from 'components/Header/index';
import Footer from 'components/Footer';
import AdvantageList from 'components/List/AdvantageList';
import Calculator from 'components/Calculator';
import Container from 'components/Container';

import BG from 'resources/images/list_example.jpg';

import colors from 'styles/colors.scss';

const ProductsOrder = () => {
  return (
    <>
      <Helmet>
        <title>RuBraVi - Product Order</title>
      </Helmet>
      <Header
        hasBanner
        image={BG}
        title='Get cloud solution'
        buttonText='Order now'
        buttonColor='neon'
        buttonLink='#content'
      />
      <div className='content-wrapper' id='content'>
        <Container>
          <Calculator />
        </Container>
        <AdvantageList
          title="What's next?"
          items={[
            {
              key: 0,
              logoColor: colors.colorNeonBright,
              title: 'Choose',
              text:
                'Choose the proper plan, which perfectly suits all your needs. Cannot find one? No worries! You can customize it!'
            },
            {
              key: 1,
              logoColor: colors.colorBlueBright,
              title: 'Fill',
              text:
                'Fill the forms on the website providing your contact details and we will keep in touch with you very soon.'
            },
            {
              key: 2,
              logoColor: colors.colorPink,
              title: 'Await',
              text:
                'Our technical consultants will make a call to finish creating the order. We will also collect all the characteristics of your personal VPC.'
            }
          ]}
          buttonText='Order Now'
          buttonLink='#content'
        />
        <Footer />
      </div>
    </>
  );
};

export default ProductsOrder;
