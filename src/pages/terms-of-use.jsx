import React from 'react';
import { Helmet } from 'react-helmet';

import './pages.scss';

import Header from 'components/Header/index';
import Footer from 'components/Footer';

import Container from 'components/Container';

class TermsOfUse extends React.Component {
  constructor() {
    super();
  }
  render() {
    return (
      <>
        <Helmet>
          <title>RuBraVi - Terms of Use</title>
        </Helmet>
        <Header />
        <div className='content-wrapper'>
          <Container>
            <h2>Terms Of Use</h2>
            <p>
              Welcome, and thank you for your interest in RuBraVi US, Inc.
              (“RuBraVi” “we,” or “us”) and our website at www.junehomes.com
              along with our related websites, and other services provided by us
              (collectively, the “Service”). Additional services provided under
              a Universal Membership Agreement (“UMA”) or Member Unit License
              (“MUL”), including terms of your Membership (as defined below) or
              licensing a RuBraVi property (each a “Property” and collectively
              “Properties”), are provided pursuant to those agreements (as
              applicable) and are not included in the Service governed by these
              Terms of Service. These Terms of Service are a legally binding
              contract between you and RuBraVi regarding your use of the
              Service. If you have executed a UMA or MUL with RuBraVi, those
              agreements govern your use of the Service to the extent their
              terms conflict with these Terms of Service.
            </p>
            <h3>PLEASE READ THE FOLLOWING TERMS CAREFULLY.</h3>
            <p>
              BY CLICKING “I ACCEPT,” OR OTHERWISE ACCESSING OR USING THE
              SERVICE, YOU AGREE THAT YOU HAVE READ AND UNDERSTOOD, AND, AS A
              CONDITION TO YOUR USE OF THE SERVICE, YOU AGREE TO BE BOUND BY,
              THE FOLLOWING TERMS AND CONDITIONS, INCLUDING RUBRAVI PRIVACY
              POLICY (TOGETHER, THESE “TERMS”). If you are not eligible, or do
              not agree to the Terms, then you do not have our permission to use
              the Service. YOUR USE OF THE SERVICE, AND RUBRAVI’ PROVISION OF
              THE SERVICE TO YOU, CONSTITUTES AN AGREEMENT BY RUBRAVI AND BY YOU
              TO BE BOUND BY THESE TERMS.
            </p>
            <p>
              Arbitration NOTICE. Except for certain kinds of disputes described
              in Section 2, and subject to Section 3, you agree that disputes
              arising under these Terms will be resolved by binding, individual
              arbitration, and BY ACCEPTING THESE TERMS, YOU AND RUBRAVI ARE
              EACH WAIVING THE RIGHT TO A TRIAL BY JURY OR TO PARTICIPATE IN ANY
              CLASS ACTION OR REPRESENTATIVE PROCEEDING. YOU AGREE TO GIVE UP
              YOUR RIGHT TO GO TO COURT to assert or defend your rights under
              this contract (except for matters that may be taken to small
              claims court). Your rights will be determined by a NEUTRAL
              ARBITRATOR and NOT a judge or jury. (See Section 2.)
            </p>
            <ol>
              <li>
                RuBraVi Service Overview. The Service provides information about
                Properties available to RuBraVi Members (as defined below) to
                license. It can be used by Members and visitors of the website
                to learn about available Properties, the Membership application
                process, submit Membership applications, and book in-person
                tours or take self-guided virtual tours of the Properties.
              </li>
              <li>
                Eligibility. You must be at least 18 years old to use the
                Service. By agreeing to these Terms, you represent and warrant
                to us that: (a) you are at least 18 years old; (b) you have not
                previously been suspended or removed from the Service; and (c)
                your registration and your use of the Service is in compliance
                with any and all applicable laws and regulations. If you are an
                entity, organization, or company, the individual accepting these
                Terms on your behalf represents and warrants that they have
                authority to bind you to these Terms and you agree to be bound
                by these Terms. Accounts and Membership Application. To access
                certain features of the Service, you may be required to provide
                contact information, including your name and email address as
                part of your application to become a member of the RuBraVi
                Community (“Member(s)” and “Membership”). You must be a Member
                of the RuBraVi community to be eligible for additional services,
                including licensing a Property and receiving promotions RuBraVi
                may offer from time to time. Information and fees provided in
                connection with applying for membership or licensing a Property
                are subject to the terms set forth in the UMA and MUL, as
                applicable. You agree that the information you provide to us is
                accurate and that you will keep it accurate and up-to-date at
                all times. Licenses
              </li>
              <li>
                Limited License. Subject to your complete and ongoing compliance
                with these Terms, RuBraVi grants you, solely for your personal,
                non-commercial use, a limited, non-exclusive, non-transferable,
                non-sublicensable, revocable license to access and use the
                Service.
              </li>
              <li>
                License Restrictions. Except and solely to the extent such a
                restriction is impermissible under applicable law, you may not:
                (a) reproduce, distribute, publicly display, or publicly perform
                the Service; (b) make modifications to the Service; or (c)
                interfere with or circumvent any feature of the Service,
                including any security or access control mechanism. If you are
                prohibited under applicable law from using the Service, you may
                not use it.
              </li>
              <li>
                Feedback. If you choose to provide input and suggestions
                regarding problems with or proposed modifications or
                improvements to the Service (“Feedback”), then you hereby grant
                RuBraVi an unrestricted, perpetual, irrevocable, non-exclusive,
                fully-paid, royalty-free right to exploit the Feedback in any
                manner and for any purpose, including to improve the Service and
                create other products and services. Ownership; Proprietary
                Rights. The Service is owned and operated by RuBraVi. The visual
                interfaces, graphics, design, compilation, information, data,
                computer code (including source code or object code), products,
                software, services, and all other elements of the Service
                (“Materials”) provided by RuBraVi are protected by intellectual
                property and other laws. All Materials included in the Service
                are the property of RuBraVi or its third party licensors. Except
                as expressly authorized by RuBraVi, you may not make use of the
                Materials. RuBraVi reserves all rights to the Materials not
                granted expressly in these Terms.
              </li>
            </ol>
          </Container>
          <Footer />
        </div>
      </>
    );
  }
}

export default TermsOfUse;
