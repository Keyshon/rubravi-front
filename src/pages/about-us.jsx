import React from 'react';
import { Helmet } from 'react-helmet';

import './pages.scss';
import Rei from 'resources/images/about-us/rei.jpg';
import Alex from 'resources/images/about-us/alex.jpg';

import Header from 'components/Header/index';
import Footer from 'components/Footer';

import SingleProductBlock from 'components/ProductBlock/SingleProductBlock';
import Container from 'components/Container';
import Button from 'components/Button';

class AboutUs extends React.Component {
  constructor() {
    super();
  }
  render() {
    return (
      <>
        <Helmet>
          <title>RuBraVi - About Us</title>
        </Helmet>
        <Header />
        <div className='content-wrapper'>
          <SingleProductBlock
            header='Rei'
            description='As a Manager at Rubravi, I have over 5 years’ experience in 
            the technology industry, especially in networking.
            I am passionate about managing and growing successful IT infrastructure 
            companies. Technology is a business enabler and it’s a huge important 
            that all businesses, no matter how big or small are provided with 
            leading-edge robust solutions to support their business growth.'
            descriptionAddition='As a network administrator, I would love to analyze 
            network traffic, install and maintain the necessary hardware and 
            software to meet those needs and solve any problems that arise along the 
            way.'
            image={Rei}
          />
          <SingleProductBlock
            header='Alex'
            description='When I just come to New Zealand I worked in hospitality to improve my 
            communications skills and to be familiar with the Kiwi environment. 
            Moreover, I have been doing a volunteer job as an I.T Service Desk 
            Analyst at Aspire2 International to gain a New Zealand experience. Looking back, 
            I can feel the confedence I have got from these jobs.'
            descriptionAddition='I am a highly motivated professional with 
            experience in office work, dedicated to improving myself to overcome 
            daily challenges and to think outside the box for possible outcomes and 
            solutions. Furthermore, I am an experienced professional, self-motivated 
            and a team player.'
            image={Alex}
            rightText
          />
          <Container>
            <h2>Meet the team</h2>
            <p className='page-about-us-prebutton-indent'>
              Our team is your team. When your mission is to be better, faster
              and smarter, you need the best people driving your vision forward.
              You need people who can create focused marketing strategies that
              align with business goals, who can infuse their creativity into
              groundbreaking campaigns, and who can analyze data to optimize
              every tactic along the way. You need RuBraVi.
            </p>
            <Button text='Visit us' link='/contact' />
          </Container>
          <Footer />
        </div>
      </>
    );
  }
}

export default AboutUs;
