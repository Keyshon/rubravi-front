import React from 'react';
import { Helmet } from 'react-helmet';

import './pages.scss';

import Header from 'components/Header/index';
import Footer from 'components/Footer';

import Contact from 'components/Contact';

class ContactUs extends React.Component {
  constructor() {
    super();
  }
  render() {
    return (
      <>
        <Helmet>
          <title>RuBraVi - Contact Us</title>
        </Helmet>
        <Header />
        <div className='content-wrapper'>
          <Contact />
          <Footer />
        </div>
      </>
    );
  }
}

export default ContactUs;
