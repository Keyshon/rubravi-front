import React from 'react';
import { Helmet } from 'react-helmet';

import colors from 'styles/colors.scss';

import Header from 'components/Header';
import Footer from 'components/Footer';

import AdvantageList from 'components/List/AdvantageList';
import ProductList from 'components/List/ProductList';
import GlobalSVG from 'resources/svg/advantages/global.svg';
import ShareableSVG from 'resources/svg/advantages/shareable.svg';
import ExandableSVG from 'resources/svg/advantages/expandable.svg';
import SecureSVG from 'resources/svg/advantages/secure.svg';
import ListBG from 'resources/images/list_example.jpg';
import VPCNetwork from 'resources/images/products/vpc-network.png';
import CloudDNS from 'resources/images/products/cloud-dns.png';

const Home = () => {
  return (
    <>
      <Helmet>
        <title>RuBraVi - Home</title>
      </Helmet>
      <Header hasBanner />
      <div className='content-wrapper'>
        <AdvantageList
          title='A Private Space within RuBraVi Cloud Platform'
          items={[
            {
              key: 0,
              logo: GlobalSVG,
              logoColor: colors.colorBlueHover,
              title: 'Global',
              text:
                "A single Google Cloud VPC can span multiple regions without communicating across the public Internet. For on-premises scenarios, you can share a connection between VPC and on-premises resources with all regions in a single VPC. You don't need a connection in every region."
            },
            {
              key: 1,
              logo: ShareableSVG,
              logoColor: colors.colorPink,
              title: 'Shareable',
              text:
                'With a single VPC for an entire organization, teams can be isolated within projects, with separate billing and quotas, yet still maintain a shared private IP space and access to commonly used services such as VPN or Cloud Interconnect.'
            },
            {
              key: 2,
              logo: ExandableSVG,
              title: 'Expandable',
              text:
                'RuBraVi Cloud VPCs let you increase the IP space of any subnets without any workload shutdown or downtime. This gives you flexibility and growth options to meet your needs.'
            },
            {
              key: 3,
              logo: SecureSVG,
              logoColor: colors.colorYellowFaint,
              title: 'Private',
              text:
                'Get private access to Google services, such as storage, big data, analytics, or machine learning, without having to give your service a public IP address. Configure your application’s front end to receive Internet requests and shield your back-end services from public endpoints.'
            }
          ]}
          advantage
          buttonText='Order Now'
          buttonColor='blue'
          buttonLink='/products/order'
        />
        <AdvantageList
          title="What's next?"
          items={[
            {
              key: 0,
              title: 'Choose',
              text:
                'Choose the proper plan, which perfectly suits all your needs. Cannot find one? No worries! You can customize it!'
            },
            {
              key: 1,
              title: 'Fill',
              text:
                'Fill the forms on the website providing your contact details and we will keep in touch with you very soon.'
            },
            {
              key: 2,
              title: 'Await',
              text:
                'Our technical consultants will make a call to finish creating the order. We will also collect all the characteristics of your personal VPC.'
            }
          ]}
          darken={40}
          smooth
          background={ListBG}
          buttonText='Order Now'
          buttonLink='/products/order'
          parallax
        />
        <ProductList
          title='Our Products'
          darken={0}
          smooth
          items={[
            {
              key: 0,
              image: VPCNetwork,
              link: '/products',
              name: 'Virtual Private Cloud',
              color: colors.colorPinkBright,
              text:
                'Provision, connect, or isolate RuBraVi Cloud Platform resources using the Google global network. Define fine-grained networking policies with Google Cloud Platform, on-premises, or public cloud infrastructure. VPC network includes granular IP address range selection, routes, firewall, Cloud VPN (Virtual Private Network), and Cloud Router.'
            },
            {
              key: 1,
              image: CloudDNS,
              link: '/products',
              name: 'Cloud DNS',
              color: colors.colorBlueFaint,
              text:
                'Cloud DNS is a scalable, reliable, programmable, and managed authoritative domain naming system (DNS) service running on the same infrastructure as Google. Cloud DNS translates domain names like www.google.com into IP addresses like 74.125.29.101. Use our simple interface, a command-line, or API to publish and manage millions of DNS zones and records.'
            }
          ]}
        />
        <Footer />
      </div>
    </>
  );
};

export default Home;
