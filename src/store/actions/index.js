export const setSubscription = value => {
  return {
    type: 'SET_SUBSCRIPTION',
    payload: value
  };
};

export const setProduct = value => {
  return {
    type: 'SET_PRODUCT',
    payload: value
  };
};

export const setTerm = value => {
  return {
    type: 'SET_TERM',
    payload: value
  };
};
