const term = (state = '', action) => {
  switch (action.type) {
    case 'SET_TERM':
      return action.payload;
    default:
      return state;
  }
};

export default term;
