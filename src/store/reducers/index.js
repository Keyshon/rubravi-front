import product from './product';
import subscription from './subscription';
import term from './term';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  product,
  subscription,
  term
});

export default rootReducer;
