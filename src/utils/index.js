const switchScrollBlocker = value => {
  document.body.style.overflow = value ? 'hidden' : 'auto';
  document.body.style.height = value ? '100%' : 'auto';

  const contentWrapper = document.getElementsByClassName('content-wrapper');
  if (contentWrapper.length > 0) {
    const height =
      window.innerHeight -
      document.getElementsByClassName('header-wrapper')[0].scrollHeight;
    contentWrapper[0].style.overflow = value ? 'hidden' : '';
    contentWrapper[0].style.height = value ? `${height}px` : '';
  }
};

const fillHeightContainer = () => {
  const headerHeight = document.getElementsByClassName('header-wrapper')[0]
    .clientHeight;
  const footerHeight = document.getElementsByClassName('footer')[0]
    .clientHeight;
  const offsetHeight = headerHeight + footerHeight + 201;

  if (window.innerHeight > offsetHeight)
    return window.innerHeight - offsetHeight;
  else return 'auto';
};

export { switchScrollBlocker, fillHeightContainer };
